/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.IOException;
import java.io.PrintWriter;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;

/**
 *
 * @author Ionut
 */
public class ClientStart2 {
    
    private ClientStart2()
    {
    
    }
    public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		MailService mailService = new MailService("your_account_here","your_password_here");
		String message;
                int k=1;
		while(true) {
			try {
				message = queue.readMessage();
                                
                                PrintWriter writer = new PrintWriter("message"+k+".txt", "UTF-8");
                                writer.println(message);
                                writer.close();
                                k++;
                                
				//System.out.println("Sending mail "+message);
				//mailService.sendMail("to_mail_address","Dummy Mail Title",message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
