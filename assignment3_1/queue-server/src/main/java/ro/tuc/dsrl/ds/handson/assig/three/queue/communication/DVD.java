/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ro.tuc.dsrl.ds.handson.assig.three.queue.communication;

/**
 *
 * @author Ionut
 */
public class DVD {
    private String title;
    private int year;
    private double price;
    
    public DVD(String title, int year , double price)
    {
      this.title=title;
      this.year=year;
      this.price=price;
    }
    
    @Override
    public String toString()
    {
     return "title: "+title+" year: "+year+" price: "+price;
    }
    
}
