/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 app.controller('usersCtrl', ['$scope','$window', '$cookies','usersFactory', 
        function ($scope, $window, $cookies, usersFactory) {
          
           // $scope.error1=""; 
           //  $scope.username2=$cookies.get("user");
           //  $cookies.remove("lv");
             function register(data){
  usersFactory.register(data)
   .then(
       function(response){
         // success callback
           $window.location='/DSClientProject//store.html';
       }, 
       function(response){
         // failure callback
           $window.location.reload();
           $scope.error1="Register failed...";
       }
    );
  }
  
  $scope.register=function(username,password,email,address){
      var data={};
      data.userName=username;
      data.password=password;
      data.email=email;
      data.level=1;
      data.address=address;
      register(data);
  };
  
   function login(data){
  usersFactory.login(data)
   .then(
       function(response){
         // success callback
          //  $window.location='/DSClientProject//store.html';
            var lev=response.data.password;
            if(lev==="0")
            {
                $window.location.reload();
                $scope.error1="Wrong username/password!!!";
            }       
           else{
            $cookies.put("lvl",response.data.password);
            $cookies.put("user",response.data.username);
            $scope.error1="Success!";
        }
       }, 
       function(response){
         // failure callback
           $window.location.reload();
          $scope.error1="Login failed...";
       }
    );
  }
  
  $scope.login=function(username,password){
    var data={};
    data.username=username;
    data.password=password;
    login(data);
  };
  
   
  $scope.logout=function(){
      $window.location.reload();
    $cookies.remove("user");
    $cookies.remove("lvl");
  };
  
  
  $scope.checkA=function()
  {
     var level=$cookies.get("lvl");
    if(level!=="2") 
    {$window.location='/DSClientProject//index.html';} 
  };
  
  
  $scope.checkU=function()
  {
    var levelus=$cookies.get("lvl");
    if(levelus!=="1")
    $window.location='/DSClientProject//index.html';
  };
}]);
