/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var productsController=angular.module('productsController',[]);

  app.controller('productsCtrl', ['$scope','$window', 'dataFactory', 
        function ($scope, $window,  dataFactory) {

    
    getProducts();
    function getProducts() {
        dataFactory.getProducts()
            .then(function (response) {
                $scope.products = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
    getMemories();
    function getMemories() {
        dataFactory.getMemories()
            .then(function (response) {
                $scope.memories = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
     getHdds();
    function getHdds() {
        dataFactory.getHdds()
            .then(function (response) {
                $scope.hdds = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
     getProcessors();
    function getProcessors() {
        dataFactory.getProcessors()
            .then(function (response) {
                $scope.processors = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
     getGraphics();
    function getGraphics() {
        dataFactory.getGraphics()
            .then(function (response) {
                $scope.graphics = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    //var testPrice;
  // getPrice(2,testPrice);
    function getPrice(id,item) {
        var data1;
        dataFactory.getPrice(id)
            .then(function (response) {
                 data1= response.data;
                 item.price=response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            }).then(function()
            {
          //$scope.price=data1;      
       });
    }
   
    $scope.setPriceMemories = function(item) {
       getPrice(item.idMemories,item); 
  };
  
   $scope.setPriceProcessors = function(item) {
       getPrice(item.idProcessor,item); 
  };
  
   $scope.setPriceHdds = function(item) {
       getPrice(item.idHdds,item); 
  };
  
   $scope.setPriceGraphics = function(item) {
       getPrice(item.idGraphicsCards,item); 
  };
  
    $scope.sortType="idProduct";
    $scope.sortReverse=false;
  
}]);