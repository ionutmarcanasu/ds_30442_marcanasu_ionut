/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var services1=angular.module('services1',[]);
 
 app.factory('dataFactory', ['$http', function($http) {

    var urlBase = "http://localhost:8080/WebProject1/";
    var dataFactory = {};

    dataFactory.getProducts = function () {
        return $http.get(urlBase+"spring1/getProducts");
    };

    dataFactory.getMemories = function () {
        return $http.get(urlBase+"spring1/getMemories");
    };
    
     dataFactory.getProcessors = function () {
        return $http.get(urlBase+"spring1/getProcessors");
    };
    
     dataFactory.getHdds = function () {
        return $http.get(urlBase+"spring1/getHdds");
    };
    
     dataFactory.getGraphics = function () {
        return $http.get(urlBase+"spring1/getGraphics");
    };
    
    dataFactory.getPrice = function (idPrice) {
        return $http.get(urlBase+"spring1/getPrice?id="+idPrice);
    };
    
    return dataFactory;
}]);

app.factory('adminFactory', ['$http', function($http) {

    var urlBase = "http://localhost:8080/WebProject1/";
    var adminFactory = {};
    var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

    adminFactory.setProduct = function (data,link) {
        return $http.post(urlBase+"spring2/"+link, data, config);
    };
    
     adminFactory.deleteProduct = function (id) {
        return $http.delete(urlBase+"spring2/deleteProduct?id="+id);
    };
    
     adminFactory.deleteOrder = function (id) {
        return $http.delete(urlBase+"spring2/deleteOrder?id="+id);
    };
    
     adminFactory.deleteUser = function (username) {
        return $http.delete(urlBase+"spring2/deleteUser?username="+username);
    };
    
      adminFactory.getOrders= function () {
        return $http.get(urlBase+"spring2/getOrders");
    };
    
      adminFactory.getUsers= function () {
        return $http.get(urlBase+"spring2/getUsers");
    };
    
     adminFactory.getOrdersDetails= function () {
        return $http.get(urlBase+"spring2/getOrdersDetails");
    };
    
    return adminFactory;
}]);

app.factory('usersFactory', ['$http' , function($http) {
       
    var urlBase = "http://localhost:8080/WebProject1/";
    var usersFactory = {};
    var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };
        usersFactory.register = function(data) {
          return $http.post(urlBase+"spring1/register",data,config);
        };
        
         usersFactory.login = function(data) {
          return $http.post(urlBase+"spring1/logIn",data,config);
        };
        
         usersFactory.addOrder = function(data) {
          return $http.post(urlBase+"spring1/addOrder",data,config);
        };
        
                 usersFactory.totalPrice = function(data) {
          return $http.post(urlBase+"spring1/findTotalPrice",data,config);
        };

      return usersFactory;
}]);