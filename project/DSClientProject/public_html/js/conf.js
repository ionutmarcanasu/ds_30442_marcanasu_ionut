/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.config(function($routeProvider,$httpProvider) {
    
     $httpProvider.defaults.withCredentials=true;
    var temp="productsTemplate/";
    $routeProvider
    .when("/products", {
        templateUrl : temp+"products.html",
        controller : "productsCtrl"
    })
    .when('/memories', {
        templateUrl : temp+"memories.html",
        controller : "productsCtrl"
    })
    .when("/processors", {
        templateUrl : temp+"processors.html",
        controller : "productsCtrl"
    })
     .when("/hdds", {
        templateUrl : temp+"hdds.html",
        controller : "productsCtrl"
    })
     .when("/graphics", {
        templateUrl : temp+"graphics.html",
        controller : "productsCtrl"
    })
     .when("/orders", {
        templateUrl : temp+"orders.html",
        controller : "crudCtrl"
    })
     .when("/ordersDetails", {
        templateUrl : temp+"ordersDetails.html",
        controller : "crudCtrl"
    })
     .when("/users", {
        templateUrl : temp+"users.html",
        controller : "crudCtrl"
    })
    .otherwise({
        template : "<div style=\"text-align:center;\"><h1>None</h1><p>Nothing has been selected</p></div>"
       // redirectTo: '/memories'
    });
});
