/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 app.controller('shopCtrl', ['$scope','$rootScope','$window', '$cookies','usersFactory', 
        function ($scope, $window,$rootScope, $cookies, usersFactory) {
          
            $scope.error1=""; 
             $scope.username2=$cookies.get("user");
           //  $cookies.remove("lv");
            
    
  
  function addOrder(data){
  usersFactory.addOrder(data)
   .then(
       function(response){
         // success callback
            $window.location='/DSClientProject//store.html';          
       }, 
       function(response){
         // failure callback
           $window.location.reload();
          $scope.error1="Add Order failed...";
       }
    );
  }
  
  $scope.addOrder=function() {
      var data={};
       
      data.idOrders=1;
      data.userName=$cookies.get("user");
      data.price=1;
      var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 
      today = dd+'/'+mm+'/'+yyyy;
      data.date=today;
      //data.products=[];
      data.products=$scope.products;
        $scope.products=[];
    // $rootScope.ddr=112;
      addOrder(data);
  };
  
  $scope.products=[];
  
   function findTotalPrice(data){
  usersFactory.totalPrice(data)
   .then(
       function(response){
         // success callback
            $scope.totalPrice=response.data.idp;          
       }, 
       function(response){
         // failure callback
          $scope.totalPrice=0;
          $scope.error1="find total price failed...";
       }
    );
  }
  
  
  function addProduct(id,quantity)
  {
      var prod1={};
      prod1.idp=id;
      prod1.quantity=quantity;
      $scope.products.push(prod1);
      findTotalPrice($scope.products);
  }
  
  $scope.addProduct=function(id,quantity){
      addProduct(id,quantity);
  };
  
  function deleteProduct(id)
  {
      for(var i = $scope.products.length - 1; i >= 0; i--) {
    if($scope.products[i].idp === id) {
       $scope.products.splice(i, 1);
    }
}
    findTotalPrice($scope.products);
  }
 
  $scope.deleteProduct=function(id)
  {
      deleteProduct(id);
  };
     
}]);

