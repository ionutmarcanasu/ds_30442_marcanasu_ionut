/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var productsController=angular.module('productsController',[]);

  app.controller('crudCtrl', ['$scope','$cookies','$window','$http', 'adminFactory', 
        function ($scope,$cookies, $window,$http, adminFactory) {

   
 // $scope.level=3;
  $scope.checkAdmin=function()
  {  var level=$cookies.get("lvl");
    if(level!=="2")
      $window.location='/DSClientProject//index.html';
  };
  
 
  function addProduct(data,link){
  adminFactory.setProduct(data,link)
   .then(
       function(response){
         // success callback
          $window.location.reload();
       }, 
       function(response){
         // failure callback
           $window.location.reload();
       }
    );
  }
  
  $scope.addProduct=function(id,price,quantity){
      var data={};
      data.idProduct=id;
      data.price=price;
      data.quantity=quantity;
      addProduct(data,"addProduct");
  };
  
  function deleteProduct(id){
   adminFactory.deleteProduct(id)
   .then(
       function(response){
         // success callback
           $window.location.reload();
       }, 
       function(response){
         // failure callback
           $window.location.reload();
       }
    );
  }
  
  $scope.deleteProduct=function(id){
      deleteProduct(id);
  };
  
  function deleteOrder(id){
   adminFactory.deleteOrder(id)
   .then(
       function(response){
         // success callback
           $window.location.reload();
       }, 
       function(response){
         // failure callback
           $window.location.reload();
       }
    );
  }
  
  $scope.deleteOrder=function(id){
      deleteOrder(id);
  };
  
  function deleteUser(username){
   adminFactory.deleteUser(username)
   .then(
       function(response){
         // success callback
           $window.location.reload();
       }, 
       function(response){
         // failure callback
           $window.location.reload();
       }
    );
  }
  
  $scope.deleteUser=function(username){
      deleteUser(username);
  };
  
  $scope.addMemories=function(id,manufacturer,model,ram,frequency){
      var data={};
      data.idMemories=id;
      data.manufacturer=manufacturer;
      data.model=model;
      data.ram=ram;
      data.frequency=frequency;
      addProduct(data,"addMemories");
  };
  
   $scope.addHdds=function(id,manufacturer,model,memory,rpms){
      var data={};
      data.idHdds=id;
      data.manufacturer=manufacturer;
      data.model=model;
      data.memory=memory;
      data.rpms=rpms;
      addProduct(data,"addHdds");
  };
  
   $scope.addProcessors=function(id,manufacturer,model,frequency,cache){
      var data={};
      data.idProcessor=id;
      data.manufacturer=manufacturer;
      data.model=model;
      data.frequency=frequency;
      data.cache=cache;
      addProduct(data,"addProcessor");
  };
  
  $scope.addGraphics=function(id,manufacturer,chipset,model,memory,memoryType,bus,clockSpeed){
      var data={};
      data.idGraphicsCards=id;
      data.manufacturer=manufacturer;
      data.chipset=chipset;
      data.model=model;
      data.memory=memory;
      data.memoryType=memoryType;
      data.bus=bus;
      data.clockSpeed=clockSpeed;
      addProduct(data,"addGraphics");
  };
  
   getOrders();
    function getOrders() {
        adminFactory.getOrders()
            .then(function (response) {
                $scope.orders = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
        }  
    getOrdersDetails();
    function getOrdersDetails() {
        adminFactory.getOrdersDetails()
            .then(function (response) {
                $scope.ordersDetails = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
    
    getUsers();
    function getUsers()
    {
         adminFactory.getUsers()
            .then(function (response) {
                $scope.users = response.data;
            }, function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });   
    }
   
  $scope.data2=new Date();
}]);
