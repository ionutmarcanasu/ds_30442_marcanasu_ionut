CREATE DATABASE  IF NOT EXISTS `dsproject1` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dsproject1`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: dsproject1
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `graphicscards`
--

DROP TABLE IF EXISTS `graphicscards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graphicscards` (
  `idGraphicsCards` int(11) NOT NULL,
  `Manufacturer` varchar(45) DEFAULT NULL,
  `Chipset` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `Memory` int(11) DEFAULT NULL,
  `MemoryType` varchar(45) DEFAULT NULL,
  `BUS` int(11) DEFAULT NULL,
  `ClockSpeed` int(11) DEFAULT NULL,
  PRIMARY KEY (`idGraphicsCards`),
  UNIQUE KEY `idGraphicsCards_UNIQUE` (`idGraphicsCards`),
  CONSTRAINT `graphicId` FOREIGN KEY (`idGraphicsCards`) REFERENCES `pcparts` (`idProduct`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graphicscards`
--

LOCK TABLES `graphicscards` WRITE;
/*!40000 ALTER TABLE `graphicscards` DISABLE KEYS */;
INSERT INTO `graphicscards` VALUES (16,'Gigabyte','AMD','RX460',4,'GDDR5',128,1250),(17,'Gigabyte','AMD','RX470',4,'GDDR5',256,1230),(18,'Inno3d','Nvidia','GT1050Ti',4,'GDDR5',128,1290),(19,'MSI','Nvidia','GTX1060',6,'GDDR5',192,1809),(20,'ASUS','Nvidia','GTX1080',8,'GDDR5X',256,1733);
/*!40000 ALTER TABLE `graphicscards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hdds`
--

DROP TABLE IF EXISTS `hdds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hdds` (
  `idHDDs` int(11) NOT NULL,
  `Manufacturer` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `Memory` int(11) DEFAULT NULL,
  `RPMs` int(11) DEFAULT NULL,
  PRIMARY KEY (`idHDDs`),
  UNIQUE KEY `idHDDs_UNIQUE` (`idHDDs`),
  CONSTRAINT `idHdds` FOREIGN KEY (`idHDDs`) REFERENCES `pcparts` (`idProduct`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hdds`
--

LOCK TABLES `hdds` WRITE;
/*!40000 ALTER TABLE `hdds` DISABLE KEYS */;
INSERT INTO `hdds` VALUES (6,'Seagate','Barracuda',1,7200),(7,'Toshiba','DT01ACA',1,7200),(8,'WesternDigital','RED',3,5400),(9,'Seagate','Barracuda',2,7200),(10,'WesternDigital','Blue',1,7200);
/*!40000 ALTER TABLE `hdds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memories`
--

DROP TABLE IF EXISTS `memories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memories` (
  `idMemories` int(11) NOT NULL,
  `Manufacturer` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `RAM` int(11) DEFAULT NULL,
  `Frequency` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMemories`),
  UNIQUE KEY `idMemories_UNIQUE` (`idMemories`),
  CONSTRAINT `idmem` FOREIGN KEY (`idMemories`) REFERENCES `pcparts` (`idProduct`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memories`
--

LOCK TABLES `memories` WRITE;
/*!40000 ALTER TABLE `memories` DISABLE KEYS */;
INSERT INTO `memories` VALUES (1,'Kingston','ValueRam',4,1333),(2,'Corsair','ValueSelect',4,2133),(3,'HyperX','SavageRed',4,1600),(4,'Corsair','Vengeance',8,1600),(5,'HyperX','FuryBlack',8,2400);
/*!40000 ALTER TABLE `memories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `idOrders` int(11) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `Price` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`idOrders`),
  KEY `idUsers_idx` (`UserName`),
  CONSTRAINT `orderName` FOREIGN KEY (`UserName`) REFERENCES `users` (`UserName`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'Alex',240,'2017-01-04'),(2,'Alex',400,'2017-01-17');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordersdetails`
--

DROP TABLE IF EXISTS `ordersdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordersdetails` (
  `idOrdersDetails` int(11) NOT NULL AUTO_INCREMENT,
  `idOrderNo` int(11) DEFAULT NULL,
  `idProduct` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOrdersDetails`),
  UNIQUE KEY `idOrdersDetails_UNIQUE` (`idOrdersDetails`),
  KEY `idOrders_idx` (`idOrderNo`),
  KEY `idProducts_idx` (`idProduct`),
  CONSTRAINT `idOrder` FOREIGN KEY (`idOrderNo`) REFERENCES `orders` (`idOrders`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `idProduct` FOREIGN KEY (`idProduct`) REFERENCES `pcparts` (`idProduct`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordersdetails`
--

LOCK TABLES `ordersdetails` WRITE;
/*!40000 ALTER TABLE `ordersdetails` DISABLE KEYS */;
INSERT INTO `ordersdetails` VALUES (1,1,120,2),(10,2,2,2),(11,2,1,1);
/*!40000 ALTER TABLE `ordersdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pcparts`
--

DROP TABLE IF EXISTS `pcparts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pcparts` (
  `price` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `idProduct` int(11) NOT NULL,
  PRIMARY KEY (`idProduct`),
  UNIQUE KEY `idProduct_UNIQUE` (`idProduct`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pcparts`
--

LOCK TABLES `pcparts` WRITE;
/*!40000 ALTER TABLE `pcparts` DISABLE KEYS */;
INSERT INTO `pcparts` VALUES (100,50,1),(150,70,2),(170,40,3),(130,50,4),(250,70,5),(210,30,6),(320,80,7),(430,90,8),(380,87,9),(300,33,10),(1700,60,11),(1900,54,12),(1200,80,13),(1500,95,14),(1100,49,15),(600,70,16),(800,57,17),(1600,40,18),(2300,80,19),(3400,20,20),(120,120,120);
/*!40000 ALTER TABLE `pcparts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processors`
--

DROP TABLE IF EXISTS `processors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processors` (
  `idProcessor` int(11) NOT NULL,
  `Manufacturer` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `Frequency` int(11) DEFAULT NULL,
  `Cache` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idProcessor`),
  UNIQUE KEY `idProcessor_UNIQUE` (`idProcessor`),
  CONSTRAINT `idproc` FOREIGN KEY (`idProcessor`) REFERENCES `pcparts` (`idProduct`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processors`
--

LOCK TABLES `processors` WRITE;
/*!40000 ALTER TABLE `processors` DISABLE KEYS */;
INSERT INTO `processors` VALUES (11,'Intel','SkylakeI7-6700k',4000,'8'),(12,'Intel','KabyLakeI7-7600k',3600,'8'),(13,'Intel','HaswellI5-4460',3200,'6'),(14,'AMD','FX8350',4000,'16'),(15,'AMD','A10-7850k',3700,'4');
/*!40000 ALTER TABLE `processors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `UserName` varchar(30) NOT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  UNIQUE KEY `UserName_UNIQUE` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('Alex','00','ionutmarcanasu@gmail.com',1,'Berlin'),('John','1234','jj',2,'Cluj'),('pp','oo','ll',1,'oo');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-18  8:53:41
