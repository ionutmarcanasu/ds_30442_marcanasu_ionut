/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

/**
 *
 * @author Ionut
 */
import entities.Orders;
import entities.Pcparts;
import entities.Processors;
import entities.Users;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.portlet.ModelAndView;
import pack2.ProdList;
import pack2.UserDetails;
import services.ProductsServices;
import services.UsersServices;
import org.springframework.web.bind.annotation.*;
import pack2.IntegTest;
import pack2.MyMail;
import pack2.OrdersTransfer;
import services.TransferServices;
import dto.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;


//@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/spring1")
public class SpringContentController {
	//@Autowired UserDetails userDetails;
	@RequestMapping(value="/test1",
			method=RequestMethod.GET,produces={"application/xml", "application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	UserDetails getUser() {
		UserDetails userDetails = new UserDetails();
                System.out.println("getUser1");
		userDetails.setUserName("Praveen");
		userDetails.setEmailId("praveen@gmail.com");
                 ProductsServices service=new ProductsServices();
                 
                // service.addProduct(11, 570, 60);
		return userDetails;
	}
        
        
         @RequestMapping(value="/hello",
			method=RequestMethod.GET)
             @ResponseStatus(HttpStatus.OK)
         public  @ResponseBody
	   String hello()
           {
              //Pcparts pc=new Pcparts(2,200,20);
               ProductsServices service=new ProductsServices();
                  List<Pcparts> listProducts=service.getProducts();
           
         //      if(service.checkProduct(15)==1)         
          //       service.addMemories(15, "Kingston", "Babaao",1024 ,1500 );
          //     service=new ProductsServices();
         //      service.addMemories(3, "Kingston", "Babaao",1024 ,1500 );
            return "Hello John!";
           }
           
            @RequestMapping(value="/getPrice",
			method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int getPrice(int id) {
		ProductsServices service=new ProductsServices();
		
                return service.getPrice(id);
	}
           
           @RequestMapping(value="/getProducts",
			method=RequestMethod.GET)
   
	public 
	List<PcpartsDTO> getProducts() {
		ProductsServices service=new ProductsServices();
                TransferServices serv=new TransferServices();
                List<Pcparts> listProducts=service.getProducts();
		return serv.productsToDTO(listProducts);
	}
        
         @RequestMapping(value="/getProcessors",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getProcessors() {
		ProductsServices service=new ProductsServices();
                 TransferServices serv=new TransferServices();
		return serv.processorsToDTO(service.getProcessors());
	}
         
         @RequestMapping(value="/getMemories",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getMemories() {
		ProductsServices service=new ProductsServices();
                TransferServices serv=new TransferServices();
		return serv.memoriesToDTO(service.getMemories());
	}
        
         @RequestMapping(value="/getHdds",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getHdds() {
		ProductsServices service=new ProductsServices();
                 TransferServices serv=new TransferServices();
		return serv.hddsToDTO(service.getHdds());
	}
        
         @RequestMapping(value="/getGraphics",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getGraphicsCards() {
		ProductsServices service=new ProductsServices();
                 TransferServices serv=new TransferServices();
		return serv.graphicsToDTO(service.getGraphicsCards());
	}
        
         
         @RequestMapping(value="/logIn",
			method=RequestMethod.POST, produces={"application/json"})
         @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	logInDTO logIn(HttpServletResponse response, @RequestBody logInDTO logData) {
		UsersServices uss=new UsersServices();
                String username=logData.getUsername();
                String password=logData.getPassword();
                int level=uss.logUser(username, password);
                if(level!=0)
                {
                Cookie lev=new Cookie("level",Integer.toString(level));
                lev.setMaxAge(3600);
                lev.setHttpOnly(false);
                 Cookie usern=new Cookie("username",username);
                usern.setMaxAge(3600);
                usern.setHttpOnly(false);
              //  response.addCookie(usern);
              //  response.addCookie(lev);
                }
                logData.setPassword(Integer.toString(level));
                return logData;
              //  return Integer.toString(level);
	}
        
         @RequestMapping(value="/register",
			method=RequestMethod.POST)
         @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int register(@RequestBody UsersDTO usDto) {
		UsersServices uss=new UsersServices();
                TransferServices serv=new TransferServices();
                Users us=serv.DTOtoUsers(usDto);
                int result=uss.register(us);
                return result;
	}
        
          @RequestMapping(value="/addOrder",
			method=RequestMethod.POST,produces={"application/json"})
           @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int addOrder(@RequestBody OrdersTransfer ord) {
		UsersServices uss=new UsersServices();
                // could add the ordersdetails list to the orders transfer !!!
               int idOrder= uss.addOrder(ord);
               List<Products> prodList=ord.getProducts();
               for(Products pr : prodList)
               {
                   addOrderDetails(idOrder,pr.getIdp(),pr.getQuantity());
               }
                //MyMail mail1=new MyMail();
                //mail1.sendEmail("ionutmarcanasu@gmail.com", "A new order has been confirmed \n Date: "+ord.getDate()+"\n Total cost: "+ord.getPrice());
                return idOrder;
	}
        
         @RequestMapping(value="/addOrderDetails",
			method=RequestMethod.POST)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int addOrderDetails(int idOrderNo, int idProduct, int quantity) {
		UsersServices uss=new UsersServices();
                uss.addOrderDetails(idOrderNo, idProduct, quantity);
                return 1;
	}
        
        
         @RequestMapping(value="/findTotalPrice",
			method=RequestMethod.POST , produces={"application/json"})
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	Products findTotalPrice(@RequestBody List<Products> prodList) {
		UsersServices uss=new UsersServices();
               int price1= uss.findPrice(prodList);
                Products prod1=new Products(price1,price1);
                return prod1;
	}
        
        @RequestMapping(value="/getTestMail",
			method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int getTestMail() {
              //  OrdersTransfer ord=new OrdersTransfer(11,"Alex",500,"05/05/2017");
              //  MyMail mail1=new MyMail();
              //  mail1.sendEmail("ionutmarcanasu@gmail.com", "A new order has been confirmed \n Date: "+ord.getDate()+"\n Total cost: "+ord.getPrice());
                return 1;
	}
        
         @RequestMapping(value="/getTest",
			method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int getTest() {
		IntegTest test1=new IntegTest();
		test1.runTest();
                return 1;
	}
        
         @RequestMapping(value="/getTestOrder",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	OrdersTransfer getTestOrder() {
                List<Products> prodList=new ArrayList<Products>();
                prodList.add(new Products(10,11));
                prodList.add(new Products(14,15));
		OrdersTransfer ord=new OrdersTransfer(5,"Alex",998,"11/12/2017",prodList);
                
                return ord;
	}
        
        
}
