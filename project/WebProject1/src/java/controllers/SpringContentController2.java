/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

/**
 *
 * @author Ionut
 */
import dto.*;
import entities.Graphicscards;
import entities.Hdds;
import entities.Memories;
import entities.Orders;
import entities.Pcparts;
import entities.Processors;
import java.util.List;
import pack2.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.portlet.ModelAndView;
import pack2.MyMail;
import services.ProductsServices;
import services.TransferServices;
import services.UsersServices;

@Controller
@RequestMapping("/spring2")
public class SpringContentController2 {
	//@Autowired UserDetails userDetails;
	@RequestMapping(value="/test2",
			method=RequestMethod.GET,produces={"application/xml", "application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	UserDetails getUser() {
		UserDetails userDetails = new UserDetails();
                System.out.println("getUser122");
		userDetails.setUserName("Praveen22");
		userDetails.setEmailId("praveen22@gmail.com");
                 ProductsServices service=new ProductsServices();
                 
                 //service.addProduct(11, 570, 60);
		return userDetails;
	}
        
        
         @RequestMapping(value="/hello",
			method=RequestMethod.GET)
         public  @ResponseBody
	   String hello()
           {
             // Pcparts pc=new Pcparts(2,200,20);
               ProductsServices service=new ProductsServices();
            //   service.deleteProduct(11);
             //  if(service.checkProduct(11)==1)
           //     service.addMemories(11, "Kingston", "Gaao",1024 ,1500 );
                
             //   service=new ProductsServices();
            //   service.addMemories(6, "Kingston", "Gaao",1024 ,1500 );
            return "Hello John!";
           }
           
            @RequestMapping(value="/addProduct",
			method=RequestMethod.POST,produces={"application/json"})
             @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	String addProduct(@RequestBody PcpartsDTO pcpartsDTO) {
           // String addProduct(Integer idproduct,Integer price) {
		ProductsServices service=new ProductsServices();
                TransferServices serv=new TransferServices();
                Pcparts pcparts=serv.DTOtoProduct(pcpartsDTO);
                service.addProduct(pcparts);
                return "aa";
	}
        
        
         @RequestMapping(value="/addProcessor",
			method=RequestMethod.POST)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int addProcessor(@RequestBody ProcessorsDTO prDTO) {
		ProductsServices service=new ProductsServices();
                TransferServices serv=new TransferServices();
                if(service.checkProduct(prDTO.getIdProcessor())==0)
                return 0;
                 Processors pr=serv.DTOtoProcessors(prDTO);
                 service.addProcessor(pr);
                return 1;
	}
        
         @RequestMapping(value="/addMemories",
			method=RequestMethod.POST)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int addMemories(@RequestBody MemoriesDTO mem1) {
		ProductsServices service=new ProductsServices();
                TransferServices serv=new TransferServices();
                if(service.checkProduct(mem1.getIdMemories())==0)
                return 0;
                 Memories mem2=serv.DTOtoMemories(mem1);
                 service.addMemories(mem2);
                return 1;
	}
        
        @RequestMapping(value="/addHdds",
			method=RequestMethod.POST)
         @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int addHdds(@RequestBody HddsDTO hddDTO) {
		ProductsServices service=new ProductsServices();
                 TransferServices serv=new TransferServices();
                if(service.checkProduct(hddDTO.getIdHdds())==0)
                return 0;
                  Hdds hdd=serv.DTOtoHdds(hddDTO);
                 service.addHdds(hdd);
                return 1;
	}
        
         @RequestMapping(value="/addGraphics",
			method=RequestMethod.POST)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int addGraphicscards(@RequestBody GraphicscardsDTO grDTO) {
		ProductsServices service=new ProductsServices();
                 TransferServices serv=new TransferServices();
                if(service.checkProduct(grDTO.getIdGraphicsCards())==0)
                return 0;
                Graphicscards gr=serv.DTOtoGraphicscards(grDTO);
                 service.addGraphicscards(gr);
                return 1;
	}
        
        @RequestMapping(value="/deleteUser",
			method=RequestMethod.DELETE)
         @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int deleteUser(String username) {
	    UsersServices uss=new UsersServices();
            uss.deleteUser(username);
            return 1;
	}
        
         @RequestMapping(value="/deleteProduct",
			method=RequestMethod.DELETE)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int deleteProduct(int id) {
	    ProductsServices service=new ProductsServices();
            service.deleteProduct(id);
            return 1;
	}
        
         @RequestMapping(value="/getOrders",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getOrders() {
		UsersServices service=new UsersServices();
                TransferServices serv=new TransferServices();
                return serv.ordersToDTO(service.getOrders());
	}
        
         @RequestMapping(value="/getOrders2",
			method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	String getOrders2() {
		UsersServices service=new UsersServices();
                List<Orders> ordList=service.getOrders();
                Orders ord=ordList.get(0);
                return "result: "+ord.getOrdersdetailses().size();
               // return "aa";
	}
        
          @RequestMapping(value="/getOrdersDetails",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getOrdersDetails() {
		UsersServices service=new UsersServices();
                TransferServices serv=new TransferServices();
                return serv.ordersdetailsToDTO(service.getOrdersDetails());
	}
        
          @RequestMapping(value="/getUsers",
			method=RequestMethod.GET,produces={"application/json"})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	List getUsers() {
		UsersServices service=new UsersServices();
                TransferServices serv=new TransferServices();
                return serv.usersToDTO(service.getUsers());
	}
        
         @RequestMapping(value="/deleteOrder",
			method=RequestMethod.DELETE)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int deleteOrder(int id) {
	    UsersServices uss=new UsersServices();
            uss.deleteOrder(id);
            return 1;
	}
        
         @RequestMapping(value="/deleteOrderDetails",
			method=RequestMethod.DELETE)
          @ResponseStatus(HttpStatus.OK)
	public @ResponseBody
	int deleteOrderDetails(int id) {
	    UsersServices uss=new UsersServices();
            uss.deleteOrdersDetails(id);
            return 1;
	}
        
}
