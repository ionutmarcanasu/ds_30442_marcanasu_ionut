/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack2;

/**
 *
 * @author Ionut
 */
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class MyMail {
    
    public MyMail()
    {}
    
    public void sendEmail(String address,String message2)
    {Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
                                @Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("ionutmarcanasu","9");
				}
			});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ionutmarcanasu@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(address));
			message.setSubject("A new order from the PC-Store!");
			message.setText(message2);

			Transport.send(message);

			//System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
