/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack2;

import entities.Graphicscards;
import entities.Hdds;
import entities.Memories;
import entities.Orders;
import entities.Pcparts;
import entities.Processors;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import services.ProductsServices;
import services.TransferServices;
import services.UsersServices;
import dto.*;

/**
 *
 * @author Ionut
 */
public class IntegTest {
    private static final String productsFile = "D:\\test\\products.txt";
     private static final String hddsFile = "D:\\test\\hdds.txt";
     private static final String memoriesFile = "D:\\test\\memories.txt";
        private static final String processorsFile = "D:\\test\\processors.txt";
        private static final String graphicsFile = "D:\\test\\graphics.txt";
        private static final String ordersFile = "D:\\test\\orders.txt";
        private static final String auxFile = "D:\\test\\aux1.txt";
        private String result="Test results: ";
        private int from;
        private int to;
   private ProductsServices prodServ;
   private UsersServices userServ;
   private TransferServices transfServ;
   
   public IntegTest()
   {
       prodServ=new ProductsServices();
       userServ=new UsersServices();
       transfServ=new TransferServices();
   }
    
    private  void loadProducts() {
        
		try (BufferedReader br = new BufferedReader(new FileReader(productsFile))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
                            int id=Integer.parseInt(sCurrentLine);
                            int price=Integer.parseInt(br.readLine());
                            int quantity=Integer.parseInt(br.readLine());
                            
                            PcpartsDTO pc2=new PcpartsDTO(id,price,quantity);
                            Pcparts pc=transfServ.DTOtoProduct(pc2);
                            prodServ.addProduct(pc);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    private void loadHdds()
    {
        try (BufferedReader br = new BufferedReader(new FileReader(hddsFile))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
                            int id=Integer.parseInt(sCurrentLine);
                            String manufacturer=br.readLine();
                            String model=br.readLine();
                             int memory=Integer.parseInt(br.readLine());
                            int rpms=Integer.parseInt(br.readLine());
                            HddsDTO hd2=new HddsDTO(id,manufacturer,model,memory,rpms);
                            Hdds hd=transfServ.DTOtoHdds(hd2);
                            prodServ.addHdds(hd);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
      private void loadMemories()
    {
        try (BufferedReader br = new BufferedReader(new FileReader(memoriesFile))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
                            int id=Integer.parseInt(sCurrentLine);
                            String manufacturer=br.readLine();
                            String model=br.readLine();
                             int memory=Integer.parseInt(br.readLine());
                            int freq=Integer.parseInt(br.readLine());
                            MemoriesDTO mem2=new MemoriesDTO(id,manufacturer,model,memory,freq);
                            Memories mem = transfServ.DTOtoMemories(mem2);
                            prodServ.addMemories(mem);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
      
       private void loadProcessors()
    {
        try (BufferedReader br = new BufferedReader(new FileReader(processorsFile))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
                            int id=Integer.parseInt(sCurrentLine);
                            String manufacturer=br.readLine();
                            String model=br.readLine();
                             int frequency=Integer.parseInt(br.readLine());
                             String cache=br.readLine();
                           ProcessorsDTO pro2=new ProcessorsDTO(id,manufacturer,model,frequency,cache);
                           Processors pro=transfServ.DTOtoProcessors(pro2);
                            prodServ.addProcessor(pro);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
       
        private void loadGraphics()
    {
        try (BufferedReader br = new BufferedReader(new FileReader(graphicsFile))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
                            int id=Integer.parseInt(sCurrentLine);
                            String manufacturer=br.readLine();
                             String chipset=br.readLine();
                            String model=br.readLine();
                             int memory=Integer.parseInt(br.readLine());
                              String memType=br.readLine();
                               int bus=Integer.parseInt(br.readLine());
                                 int clockSpeed=Integer.parseInt(br.readLine());
                           GraphicscardsDTO gr2=new GraphicscardsDTO(id,manufacturer,chipset,model,memory,memType,bus,clockSpeed);
                           Graphicscards gr=transfServ.DTOtoGraphicscards(gr2);
                            prodServ.addGraphicscards(gr);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
        
        
          private void loadAux()
    {
        try (BufferedReader br = new BufferedReader(new FileReader(auxFile))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
                             from=Integer.parseInt(sCurrentLine);
                             to=Integer.parseInt(br.readLine());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
         
         private void deleteData()
         {
             int initSize=prodServ.getProducts().size();
          for(int i=from;i<=to;i++)
              prodServ.deleteProduct(i);
          int finalSize=prodServ.getProducts().size();
          if((initSize-finalSize)==((to-from)+1))
               result=result+"\n DeleteTest: Successful";
         else
             result=result+"\n DeleteTest: Fail";
         }
        
        private void addData()
        {
         List<Pcparts> pcList=prodServ.getProducts();
         int initsize=pcList.size();
         loadProducts();
         pcList=prodServ.getProducts();
         int finalsize=pcList.size();
         if(((to-from)+1)==(finalsize-initsize))
             result=result+"\n ProductsLoadTest: Successful";
         else
             result=result+"\n ProductsLoadTest: Failed";
         initsize=prodServ.getHdds().size();
         loadHdds();
         finalsize=prodServ.getHdds().size();
         if((finalsize-initsize)==1)
             result=result+"\n HddsLoadTest: Successful";
         else
             result=result+"\n HddsLoadTest: Fail";
         initsize=prodServ.getMemories().size();
         loadMemories();
         finalsize=prodServ.getMemories().size();
         if((finalsize-initsize)==1)
             result=result+"\n MemoriesLoadTest: Successful";
         else
             result=result+"\n MemoriesLoadTest: Fail";
        }
        
        private void duplicateIdCategories()
        {
          List<Hdds> hdList=prodServ.getHdds();
          int idHd=hdList.get(1).getIdHdds();
          MemoriesDTO mem2=new MemoriesDTO(idHd,"aa","bb",2,3);
          Memories mem=transfServ.DTOtoMemories(mem2);
          int initSize=prodServ.getMemories().size();
          prodServ.addMemories(mem);
          int finalSize=prodServ.getMemories().size();
          if(initSize==finalSize)
               result=result+"\n DuplicateIdCategoriesTest: Successful";
         else
             result=result+"\n DuplicateIdCategoriesTest: Fail";
        }
        
      private void writeToFile()
      {
           try {
        BufferedWriter out = new BufferedWriter(new FileWriter("D:\\test\\resultTest.txt"));
                out.write(result);
            
            out.close();
        } catch (IOException e) {}
    }
      
      public void runTest()
      {
          loadAux();
          duplicateIdCategories();
          addData();
          deleteData();
          writeToFile();
      }
     
  // public void runTest()
  // {}
}
