/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack2;
import dto.Products;
import java.util.List;
/**
 *
 * @author Ionut
 */
public class OrdersTransfer implements java.io.Serializable {
     private int idOrders;
     private String userName;
     private int price;
     private String date;
     private List<Products> prods;

    public OrdersTransfer() {
    }

	
    public OrdersTransfer(int idOrders, String userName, int price, String date) {
       this.idOrders = idOrders;
       this.userName = userName;
       this.price = price;
       this.date = date;
    }
    
     public OrdersTransfer(int idOrders, String userName, int price, String date, List<Products> prods) {
       this.idOrders = idOrders;
       this.userName = userName;
       this.price = price;
       this.date = date;
       this.prods=prods;
    }
   
    public int getIdOrders() {
        return this.idOrders;
    }
    
    public void setIdOrders(int idOrders) {
        this.idOrders = idOrders;
    }
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public int getPrice() {
        return this.price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    public String getDate() {
        return this.date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public List<Products> getProducts()
    {
        return prods;
    }
    
    public void setProducts(List<Products> prodList)
    {
        this.prods=prodList;
    }
    
}
