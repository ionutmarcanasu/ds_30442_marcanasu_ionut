package entities;
// Generated Jan 11, 2017 2:30:22 PM by Hibernate Tools 4.3.1



/**
 * Hdds generated by hbm2java
 */
public class Hdds  implements java.io.Serializable {


     private int idHdds;
     private Pcparts pcparts;
     private String manufacturer;
     private String model;
     private Integer memory;
     private Integer rpms;

    public Hdds() {
    }

	
    public Hdds(Pcparts pcparts) {
        this.pcparts = pcparts;
    }
    public Hdds(Pcparts pcparts, String manufacturer, String model, Integer memory, Integer rpms) {
       this.pcparts = pcparts;
       this.manufacturer = manufacturer;
       this.model = model;
       this.memory = memory;
       this.rpms = rpms;
    }
   
    public int getIdHdds() {
        return this.idHdds;
    }
    
    public void setIdHdds(int idHdds) {
        this.idHdds = idHdds;
    }
    public Pcparts getPcparts() {
        return this.pcparts;
    }
    
    public void setPcparts(Pcparts pcparts) {
        this.pcparts = pcparts;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getMemory() {
        return this.memory;
    }
    
    public void setMemory(Integer memory) {
        this.memory = memory;
    }
    public Integer getRpms() {
        return this.rpms;
    }
    
    public void setRpms(Integer rpms) {
        this.rpms = rpms;
    }




}


