package entities;
// Generated Jan 11, 2017 2:30:22 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Pcparts generated by hbm2java
 */
public class Pcparts  implements java.io.Serializable {


     private int idProduct;
     private int price;
     private Integer quantity;
     private Processors processors;
     private Hdds hdds;
     private Graphicscards graphicscards;
     private Set ordersdetailses = new HashSet(0);
     private Memories memories;

    public Pcparts() {
    }

	
    public Pcparts(int idProduct, int price) {
        this.idProduct = idProduct;
        this.price = price;
    }
    public Pcparts(int idProduct, int price, Integer quantity, Processors processors, Hdds hdds, Graphicscards graphicscards, Set ordersdetailses, Memories memories) {
       this.idProduct = idProduct;
       this.price = price;
       this.quantity = quantity;
       this.processors = processors;
       this.hdds = hdds;
       this.graphicscards = graphicscards;
       this.ordersdetailses = ordersdetailses;
       this.memories = memories;
    }
   
    public int getIdProduct() {
        return this.idProduct;
    }
    
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
    public int getPrice() {
        return this.price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    public Integer getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Processors getProcessors() {
        return this.processors;
    }
    
    public void setProcessors(Processors processors) {
        this.processors = processors;
    }
    public Hdds getHdds() {
        return this.hdds;
    }
    
    public void setHdds(Hdds hdds) {
        this.hdds = hdds;
    }
    public Graphicscards getGraphicscards() {
        return this.graphicscards;
    }
    
    public void setGraphicscards(Graphicscards graphicscards) {
        this.graphicscards = graphicscards;
    }
    public Set getOrdersdetailses() {
        return this.ordersdetailses;
    }
    
    public void setOrdersdetailses(Set ordersdetailses) {
        this.ordersdetailses = ordersdetailses;
    }
    public Memories getMemories() {
        return this.memories;
    }
    
    public void setMemories(Memories memories) {
        this.memories = memories;
    }




}


