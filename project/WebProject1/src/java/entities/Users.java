package entities;
// Generated Jan 11, 2017 2:30:22 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Users generated by hbm2java
 */
public class Users  implements java.io.Serializable {


     private String userName;
     private String password;
     private String email;
     private Integer level;
     private String address;
     private Set orderses = new HashSet(0);

    public Users() {
    }

	
    public Users(String userName) {
        this.userName = userName;
    }
    public Users(String userName, String password, String email, Integer level, String address, Set orderses) {
       this.userName = userName;
       this.password = password;
       this.email = email;
       this.level = level;
       this.address = address;
       this.orderses = orderses;
    }
   
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public Integer getLevel() {
        return this.level;
    }
    
    public void setLevel(Integer level) {
        this.level = level;
    }
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    public Set getOrderses() {
        return this.orderses;
    }
    
    public void setOrderses(Set orderses) {
        this.orderses = orderses;
    }




}


