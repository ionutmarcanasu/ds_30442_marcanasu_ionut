/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import entities.Graphicscards;
import entities.Hdds;
import entities.Memories;
import entities.NewHibernateUtil;
import entities.Pcparts;
import entities.Processors;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class ProductsAccess {
     private Session session=null;
     
     public ProductsAccess(){
           this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
     }
      public List getProducts()
    {
     List<Pcparts> partsList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Pcparts as pcparts");
         partsList = (List<Pcparts>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return partsList;
    }
      public Pcparts getPartById(int id)
      {
         List<Pcparts> partsList=getProducts();
         for(Pcparts pc : partsList)
             if(pc.getIdProduct()==id)
                 return pc;
         return null;
      }
    
     public List getMemories()
    {
     List<Memories> partsList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Memories as memories");
         partsList = (List<Memories>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return partsList;
    }
     
      public List getProcessors()
    {
     List<Processors> partsList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Processors as processors");
         partsList = (List<Processors>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return partsList;
    }
      
        public List getHdds()
    {
     List<Hdds> partsList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Hdds as hdds");
         partsList = (List<Hdds>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return partsList;
    }
    
          public List getGraphicscards()
    {
     List<Graphicscards> partsList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Graphicscards as graphicscards");
         partsList = (List<Graphicscards>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return partsList;
    }
}
