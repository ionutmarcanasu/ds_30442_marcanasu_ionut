/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import entities.NewHibernateUtil;
import entities.Pcparts;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class CrudAccess {
     private Session session=null;
     
     public CrudAccess(){
           this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
     }
     
     public void addObject(Object obj)
     {
     try {
            this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
           // session.cancelQuery();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        } 
     }
     
       public void addPcparts(Pcparts pc)
     {
     try {
            this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
           // session.cancelQuery();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(pc);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        } 
     }
     
     public void deleteObject(Object obj)
     {
       try {
            this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.delete(obj);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
     }
     
     
}
