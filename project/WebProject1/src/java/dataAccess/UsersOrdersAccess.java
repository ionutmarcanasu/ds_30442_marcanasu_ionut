/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import entities.NewHibernateUtil;
import entities.Orders;
import entities.Ordersdetails;
import entities.Users;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class UsersOrdersAccess {
     private Session session=null;
     
     public UsersOrdersAccess(){
           this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
     }
     
      public List getUsers()
    {
     List<Users> usersList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Users as users");
         usersList = (List<Users>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return usersList;
    }
      
       public List getOrders()
    {
     List<Orders> ordersList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Orders as orders");
         ordersList = (List<Orders>) q.list();
       //  Hibernate.initialize(ordersList.get(0).getOrdersdetailses());
         // Hibernate.initialize(ordersList.get(0).getUsers());
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return ordersList;
    }
      
     public List getOrdersDetails()
    {
     List<Ordersdetails> ordersdetailsList = null;
    try {
         this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Ordersdetails as ordersdetails");
         ordersdetailsList = (List<Ordersdetails>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
     return ordersdetailsList;
    }  
     
     public Orders getOrderById(int id)
     {
         List<Orders> ordersList=getOrders();
         for(Orders ord : ordersList)
             if(ord.getIdOrders()==id)
                 return ord;
         return null;
     }
}
