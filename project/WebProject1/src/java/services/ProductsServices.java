/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import dataAccess.CrudAccess;
import dataAccess.ProductsAccess;
import entities.Graphicscards;
import entities.Hdds;
import entities.Memories;
import entities.Pcparts;
import entities.Processors;
import java.util.List;

/**
 *
 * @author Ionut
 */
public class ProductsServices {
   private CrudAccess crud=null;
   private ProductsAccess pac=null;
   private ProductsHelper phelper=null;
   
    public ProductsServices()
    {
        this.crud=new CrudAccess();
         this.pac=new ProductsAccess();
         this.phelper=new ProductsHelper();
    }
    
    
    public void addProduct(Pcparts pc)
    {
       // Pcparts pc2=new Pcparts(pc.getIdProduct(),pc.getPrice(),pc.getQuantity());
               // if(checkProduct(pc2.getIdProduct())==0)
      crud.addObject(pc);
    }
    
    public void addProcessor(Processors pr)
    {
        int id=pr.getIdProcessor();
        int a=phelper.checkGraphicscards(id, pac.getGraphicscards());
        int b=phelper.checkHdds(id, pac.getHdds());
        int c=phelper.checkMemories(id, pac.getMemories());
        int d=a+b+c;
        if(d!=0)
            return;
        crud.addObject(pr);
         
    }
    
    public void addMemories(Memories mem)
    {
        int id=mem.getIdMemories();
         int a=phelper.checkGraphicscards(id, pac.getGraphicscards());
        int b=phelper.checkHdds(id, pac.getHdds());
        int c=phelper.checkProcessors(id, pac.getProcessors());
        int d=a+b+c;
        if(d!=0)
            return;
         crud.addObject(mem);
    }
    
    public void addHdds(Hdds hdd)
    {    int idHdds=hdd.getIdHdds();
         int a=phelper.checkGraphicscards(idHdds, pac.getGraphicscards());
        int b=phelper.checkProcessors(idHdds, pac.getProcessors());
        int c=phelper.checkMemories(idHdds, pac.getMemories());
        int d=a+b+c;
        if(d!=0)
            return;
         crud.addObject(hdd);
    }
    
     public void addGraphicscards(Graphicscards gr)
    {  
         int id=gr.getIdGraphicsCards();
         int a=phelper.checkProcessors(id, pac.getProcessors());
        int b=phelper.checkHdds(id, pac.getHdds());
        int c=phelper.checkMemories(id, pac.getMemories());
        int d=a+b+c;
        if(d!=0)
            return;
        
         crud.addObject(gr);
    }
    
   
          
    public Pcparts getProductById(int id)
    {
        List<Pcparts> prodList=pac.getProducts();
        for(Pcparts prod : prodList)
        {
         if(prod.getIdProduct()==id)
             return prod;
        }
        return null;
    }
  
    public int checkProduct(int id)
    {
      int result=phelper.checkProduct(id, pac.getProducts());
        return result;
    }
  
    
      public List getProducts()
     {
         return pac.getProducts();
     }
    
     public List getHdds()
     {
         return pac.getHdds();
     }
     
       public List getMemories()
     {
         return pac.getMemories();
     }
       
         public List getProcessors()
     {
         return pac.getProcessors();
     }
         
          public List getGraphicsCards()
     {
         return pac.getGraphicscards();
     }
       
          public int getPrice(int id)
          {
           List<Pcparts> partsList=pac.getProducts();
           for(Pcparts pc : partsList)
               if(pc.getIdProduct()==id)
                   return pc.getPrice();
           return 0;
          }
  
            public void deleteProduct(int id)
    {    if(phelper.checkProduct(id, pac.getProducts())==1)
     { 
         Pcparts prod=getProductById(id);
         crud.deleteObject(prod);
     }
    }
            public Pcparts getPartById(int id)
            {
                return pac.getPartById(id);
            }
            
}
