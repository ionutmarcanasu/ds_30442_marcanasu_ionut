/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import dataAccess.CrudAccess;
import dataAccess.ProductsAccess;
import dataAccess.UsersOrdersAccess;
import dto.Products;
import entities.NewHibernateUtil;
import entities.Orders;
import entities.Ordersdetails;
import entities.Pcparts;
import entities.Users;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import pack2.OrdersTransfer;

/**
 *
 * @author Ionut
 */
public class UsersServices {
     private CrudAccess crud=null;
     private ProductsAccess pac=null;
     private UsersOrdersAccess uoa=null;
     
      public UsersServices()
    {
        this.crud=new CrudAccess();
        this.pac=new ProductsAccess();
        this.uoa=new UsersOrdersAccess();
    }
      
      
       
       public int logUser(String name, String password)
       {
         List<Users> usersList=uoa.getUsers();
         for(Users us : usersList)
         {
          if(us.getUserName().equals(name))
              if(us.getPassword().equals(password))
                  if(us.getLevel()==1)
                  return 1;
                  else
                      return 2;
         }
         return 0;
       }
       
        public int checkUser(String name)
       {
         List<Users> usersList=uoa.getUsers();
         for(Users us : usersList)
         {
          if(us.getUserName().equals(name))
                  return 1;
         }
         return 0;
       }
        
         public Users getUserByName(String name)
       {
         List<Users> usersList=uoa.getUsers();
         for(Users us : usersList)
         {
          if(us.getUserName().equals(name))
                  return us;
         }
         return null;
       }
       
       public void addUser(String userName, String password, String email, int level, String address)
       {
        Users us=new Users(userName,password,email,level,address,null);
       crud.addObject(us);
       }
       
      public int register(Users us)
      {
         if(checkUser(us.getUserName())==1)
             return 0;
         addUser(us.getUserName(),  us.getPassword(), us.getEmail(), 1, us.getAddress());
         return 1;
      }
       
       public void deleteUser(String userName)
       {
         if(checkUser(userName)==1)
         {
           Users us=getUserByName(userName);
           crud.deleteObject(us);
         }
       }
       
       public List getUsers()
       {
           return uoa.getUsers();
       }
       
       public List getOrders()
       {
           return uoa.getOrders();
       }
       
       public List getOrdersDetails()
       {
        return uoa.getOrdersDetails();
       }
       
       public List getOrdersByClient(String username)
       {
         List<Orders> ordersList=null;
         List<Orders> completeList=uoa.getOrders();
         for(Orders ord : completeList)
             if(ord.getUsers().getUserName().equals(username))
                 ordersList.add(ord);
        return ordersList;
       }
       
        public List getOrdersDetailsById(int id)
        {
         List<Ordersdetails> ordersDetailsList=null;
         List<Ordersdetails> completeList=uoa.getOrdersDetails();
         for(Ordersdetails ordetail : completeList)
         {
             if(ordetail.getOrders().getIdOrders()==id)
                 ordersDetailsList.add(ordetail);
         }
         return ordersDetailsList;
        }
        
        public void deleteOrder(int id)
        {
         List<Orders> completeList=uoa.getOrders();
         for(Orders ord : completeList)
             if(ord.getIdOrders()==id)
             {
              crud.deleteObject(ord);
              return;
             }
        }
        
        public void deleteOrdersDetails(int id)
        {
          List<Ordersdetails> completeList=uoa.getOrdersDetails();
          for(Ordersdetails ordetail : completeList)
              if(ordetail.getIdOrdersDetails()==id)
              {
               crud.deleteObject(ordetail);
               return;
              }
        }
        
        public int findPrice(List<Products> prodList)
        {   int price=0;
             ProductsServices prodServ=new ProductsServices();
            for(Products pr : prodList)
            {
              price+=pr.getQuantity()*prodServ.getPrice(pr.getIdp());
            }
            return price;
        }
        
        public int addOrder(OrdersTransfer ord2)
        {
            int priceFinal=findPrice(ord2.getProducts());
          List<Orders> completeList=uoa.getOrders();
          int max=0;
          for(Orders ord :  completeList)
          {
              if(ord.getIdOrders()>max)
                  max=ord.getIdOrders();
          }
          max++;
          SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
          Date date2;
         try {
             date2 = sdf.parse(ord2.getDate());
             Users us=getUserByName(ord2.getUserName());
             Orders orders=new Orders(max,us,priceFinal,date2,null);
          crud.addObject(orders);
          return max;
         } catch (ParseException ex) {
             Logger.getLogger(UsersServices.class.getName()).log(Level.SEVERE, null, ex);
         }
          return 0;
        }
        
        public void addOrderDetails(int idOrderNo, int idProduct, int quantity)
        {
            Orders ord1=uoa.getOrderById(idOrderNo);
            Pcparts pc1=pac.getPartById(idProduct);
          Ordersdetails ordetail=new Ordersdetails(ord1,pc1,quantity);
          crud.addObject(ordetail);
        }
        
        public Orders getOrderById(int id)
        {
            return uoa.getOrderById(id);
        }
}
