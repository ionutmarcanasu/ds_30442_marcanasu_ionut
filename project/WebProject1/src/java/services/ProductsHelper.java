/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package services;

import entities.Graphicscards;
import entities.Hdds;
import entities.Memories;
import entities.Pcparts;
import entities.Processors;
import java.util.List;

/**
 *
 * @author Ionut
 */
public class ProductsHelper {
    
   public ProductsHelper()
    {
    
    }
      public  int checkProduct(int id,List list)
    {
      List<Pcparts> partsList = list;
      for(Pcparts pc : partsList)
      {
          if(pc.getIdProduct()==id)
              return 1;
      }
      return 0;
    }
    
  public int checkMemories(int id,List list)
         {
      List<Memories> partsList = list;
      for(Memories pc : partsList)
      {
          if(pc.getIdMemories()==id)
              return 1;
      }
      return 0;
    } 
  
  public int checkHdds(int id,List list)
         {
      List<Hdds> partsList = list;
      for(Hdds pc : partsList)
      {
          if(pc.getIdHdds()==id)
              return 1;
      }
      return 0;
    }  
  
  public int checkProcessors(int id,List list)
         {
      List<Processors> partsList = list;
      for(Processors pc : partsList)
      {
          if(pc.getIdProcessor()==id)
              return 1;
      }
      return 0;
    }  
  
  public int checkGraphicscards(int id,List list)
         {
      List<Graphicscards> partsList = list;
      for(Graphicscards pc : partsList)
      {
          if(pc.getIdGraphicsCards()==id)
              return 1;
      }
      return 0;
    } 
}
