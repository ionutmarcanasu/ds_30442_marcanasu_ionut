/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;
import entities.*;
import dto.*;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Ionut
 */
public class TransferServices {
    private ProductsServices prs=null;
    private UsersServices uss=null;
    
    public TransferServices(){
      this.prs=new ProductsServices();
      this.uss=new UsersServices();
    }
    
  public List<PcpartsDTO> productsToDTO(List<Pcparts> prodList)
    {
    List<PcpartsDTO> partsList = new ArrayList<PcpartsDTO>();
    for(Pcparts prod : prodList)
    {
     PcpartsDTO pc=new PcpartsDTO(prod.getIdProduct(),prod.getPrice(),prod.getQuantity());
     partsList.add(pc);
    }   
    return partsList;
    }
  
  public Pcparts DTOtoProduct(PcpartsDTO pcpartsDTO)
  {
       Pcparts pc=new Pcparts(pcpartsDTO.getIdProduct(),pcpartsDTO.getPrice(),pcpartsDTO.getQuantity(),null,null,null,null,null);
       return pc;
  }
  
  public List<MemoriesDTO> memoriesToDTO(List<Memories> prodList)
  {
   List<MemoriesDTO> partsList = new ArrayList<MemoriesDTO>();
    for(Memories prod : prodList)
    {
     MemoriesDTO memories=new MemoriesDTO(prod.getIdMemories(),prod.getManufacturer(),prod.getModel(),prod.getRam(),prod.getFrequency());
     partsList.add(memories);
    }   
    return partsList;
  }
  
  public Memories DTOtoMemories(MemoriesDTO memdto)
  {
    Pcparts pc=prs.getPartById(memdto.getIdMemories());
    Memories mem=new Memories(pc,memdto.getManufacturer(),memdto.getModel(),memdto.getRam(),memdto.getFrequency());
    return mem;
  }
  
  public List<HddsDTO> hddsToDTO(List<Hdds> prodList)
  {
   List<HddsDTO> partsList = new ArrayList<HddsDTO>();
    for(Hdds prod : prodList)
    {
     HddsDTO hdds=new HddsDTO(prod.getIdHdds(),prod.getManufacturer(),prod.getModel(),prod.getMemory(),prod.getRpms());
     partsList.add(hdds);
    }   
    return partsList;
  }
  
   public Hdds DTOtoHdds(HddsDTO hdddto)
  {
    Pcparts pc=prs.getPartById(hdddto.getIdHdds());
    Hdds hdd=new Hdds(pc,hdddto.getManufacturer(),hdddto.getModel(),hdddto.getMemory(),hdddto.getRpms());
    return hdd;
  }
   
    public List<ProcessorsDTO> processorsToDTO(List<Processors> prodList)
  {
   List<ProcessorsDTO> partsList = new ArrayList<ProcessorsDTO>();
    for(Processors prod : prodList)
    {
     ProcessorsDTO processors=new ProcessorsDTO(prod.getIdProcessor(),prod.getManufacturer(),prod.getModel(),prod.getFrequency(),prod.getCache());
     partsList.add(processors);
    }   
    return partsList;
  }
   
    
   public Processors DTOtoProcessors(ProcessorsDTO prodto)
  {
    Pcparts pc=prs.getPartById(prodto.getIdProcessor());
    Processors pro=new Processors(pc,prodto.getManufacturer(),prodto.getModel(),prodto.getFrequency(),prodto.getCache());
    return pro;
  }
   
    public List<GraphicscardsDTO> graphicsToDTO(List<Graphicscards> prodList)
  {
   List<GraphicscardsDTO> partsList = new ArrayList<GraphicscardsDTO>();
    for(Graphicscards prod : prodList)
    {
     GraphicscardsDTO graphics=new GraphicscardsDTO(prod.getIdGraphicsCards(),prod.getManufacturer(),prod.getChipset(),prod.getModel(),prod.getMemory(),prod.getMemoryType(),prod.getBus(),prod.getClockSpeed());
     partsList.add(graphics);
    }   
    return partsList;
  }
    
    public Graphicscards DTOtoGraphicscards(GraphicscardsDTO prodto)
  {
    Pcparts pc=prs.getPartById(prodto.getIdGraphicsCards());
    Graphicscards gr=new Graphicscards(pc,prodto.getManufacturer(),prodto.getChipset(),prodto.getModel(),prodto.getMemory(),prodto.getMemoryType(),prodto.getBus(),prodto.getClockSpeed());
    return gr;
  }
    
      public List<UsersDTO> usersToDTO(List<Users> prodList)
  {
   List<UsersDTO> partsList = new ArrayList<UsersDTO>();
    for(Users prod : prodList)
    {
     UsersDTO users=new UsersDTO(prod.getUserName(),prod.getPassword(),prod.getEmail(),prod.getLevel(),prod.getAddress());
     partsList.add(users);
    }   
    return partsList;
  }
   
    public Users DTOtoUsers(UsersDTO prodto)
  {
    Users pro=new Users(prodto.getUserName(),prodto.getPassword(),prodto.getEmail(),prodto.getLevel(),prodto.getAddress(),null);
    return pro;
  }   
   
     public List<OrdersDTO> ordersToDTO(List<Orders> prodList)
  {
   List<OrdersDTO> partsList = new ArrayList<OrdersDTO>();
    for(Orders prod : prodList)
    {
     OrdersDTO orders=new OrdersDTO(prod.getIdOrders(),prod.getUsers().getUserName(),prod.getPrice(),prod.getDate());
     partsList.add(orders);
    }   
    return partsList;
  }
     
          public List<OrdersdetailsDTO> ordersdetailsToDTO(List<Ordersdetails> prodList)
  {
   List<OrdersdetailsDTO> partsList = new ArrayList<OrdersdetailsDTO>();
    for(Ordersdetails prod : prodList)
    {
     OrdersdetailsDTO ordersdetails=new OrdersdetailsDTO(prod.getIdOrdersDetails(),prod.getOrders().getIdOrders(),prod.getPcparts().getIdProduct(),prod.getQuantity());
     partsList.add(ordersdetails);
    }   
    return partsList;
  }

}
