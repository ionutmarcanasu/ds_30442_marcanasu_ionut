/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class OrdersdetailsDTO  implements java.io.Serializable {


     private Integer idOrdersDetails;
     private Integer idOrderNo;
     private Integer idProduct;
     private Integer quantity;

    public OrdersdetailsDTO() {
    }

    public OrdersdetailsDTO(int idOrdersDetails, Integer idOrderNo, Integer idProduct, Integer quantity) {
        this.idOrdersDetails=idOrdersDetails;
        this.idOrderNo = idOrderNo;
       this.idProduct = idProduct;
       this.quantity = quantity;
    }
   
    public Integer getIdOrdersDetails() {
        return this.idOrdersDetails;
    }
    
    public void setIdOrdersDetails(Integer idOrdersDetails) {
        this.idOrdersDetails = idOrdersDetails;
    }
    public Integer getIdOrderNo() {
        return this.idOrderNo;
    }
    
    public void setIdOrderNo(Integer idOrderNo) {
        this.idOrderNo = idOrderNo;
    }
    public Integer getIdProduct() {
        return this.idProduct;
    }
    
    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }
    public Integer getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }




}
