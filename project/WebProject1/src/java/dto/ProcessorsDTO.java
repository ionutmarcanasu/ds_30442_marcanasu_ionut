/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class ProcessorsDTO  implements java.io.Serializable {


     private int idProcessor;
     private String manufacturer;
     private String model;
     private Integer frequency;
     private String cache;

    public ProcessorsDTO() {
    }

	
    public ProcessorsDTO(int idProcessor, String manufacturer, String model, Integer frequency, String cache) {
       this.idProcessor = idProcessor;
       this.manufacturer = manufacturer;
       this.model = model;
       this.frequency = frequency;
       this.cache = cache;
    }
   
    public int getIdProcessor() {
        return this.idProcessor;
    }
    
    public void setIdProcessor(int idProcessor) {
        this.idProcessor = idProcessor;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getFrequency() {
        return this.frequency;
    }
    
    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }
    public String getCache() {
        return this.cache;
    }
    
    public void setCache(String cache) {
        this.cache = cache;
    }




}