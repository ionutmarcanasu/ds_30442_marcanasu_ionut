/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class PcpartsDTO implements java.io.Serializable {
    

     private int idProduct;
     private int price;
     private Integer quantity;

    public PcpartsDTO() {
    }

	
    public PcpartsDTO(int idProduct, int price, int quantity) {
       this.idProduct = idProduct;
       this.price = price;
       this.quantity = quantity;
    }
   
    public int getIdProduct() {
        return this.idProduct;
    }
    
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
    public int getPrice() {
        return this.price;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    public Integer getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }


}
