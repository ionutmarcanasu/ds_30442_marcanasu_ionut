/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class Products implements java.io.Serializable {
    private int idp;
    private int quantity;
    
    public Products()
    {}
    
    public Products(int idp, int quantity)
    {
        this.idp=idp;
        this.quantity=quantity;
    }
    
    public int getQuantity()
    {
     return quantity;
    }
    
    public int getIdp()
    {
        return idp;
    }
    
    public void setQuantity(int quantity)
    {
     this.quantity=quantity;
    }
    
    public void setIdp(int idp)
    {
        this.idp=idp;
    }
}
