/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class GraphicscardsDTO  implements java.io.Serializable {


     private int idGraphicsCards;
     private String manufacturer;
     private String chipset;
     private String model;
     private Integer memory;
     private String memoryType;
     private Integer bus;
     private Integer clockSpeed;

    public GraphicscardsDTO() {
    }

	
    public GraphicscardsDTO(int idGraphicsCards, String manufacturer, String chipset, String model, int memory, String memoryType, int bus, int clockSpeed) {
       this.idGraphicsCards = idGraphicsCards;
       this.manufacturer = manufacturer;
       this.chipset = chipset;
       this.model = model;
       this.memory = memory;
       this.memoryType = memoryType;
       this.bus = bus;
       this.clockSpeed = clockSpeed;
    }
   
    public int getIdGraphicsCards() {
        return this.idGraphicsCards;
    }
    
    public void setIdGraphicsCards(int idGraphicsCards) {
        this.idGraphicsCards = idGraphicsCards;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getChipset() {
        return this.chipset;
    }
    
    public void setChipset(String chipset) {
        this.chipset = chipset;
    }
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getMemory() {
        return this.memory;
    }
    
    public void setMemory(Integer memory) {
        this.memory = memory;
    }
    public String getMemoryType() {
        return this.memoryType;
    }
    
    public void setMemoryType(String memoryType) {
        this.memoryType = memoryType;
    }
    public Integer getBus() {
        return this.bus;
    }
    
    public void setBus(Integer bus) {
        this.bus = bus;
    }
    public Integer getClockSpeed() {
        return this.clockSpeed;
    }
    
    public void setClockSpeed(Integer clockSpeed) {
        this.clockSpeed = clockSpeed;
    }




}

