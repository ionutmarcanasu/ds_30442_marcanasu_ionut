/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */

public class HddsDTO  implements java.io.Serializable {


     private int idHdds;
     private String manufacturer;
     private String model;
     private Integer memory;
     private Integer rpms;

    public HddsDTO() {
    }

	
    public HddsDTO(int idHdds, String manufacturer, String model, int memory, int rpms) {
       this.idHdds = idHdds;
       this.manufacturer = manufacturer;
       this.model = model;
       this.memory = memory;
       this.rpms = rpms;
    }
   
    public int getIdHdds() {
        return this.idHdds;
    }
    
    public void setIdHdds(int idHdds) {
        this.idHdds = idHdds;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getMemory() {
        return this.memory;
    }
    
    public void setMemory(Integer memory) {
        this.memory = memory;
    }
    public Integer getRpms() {
        return this.rpms;
    }
    
    public void setRpms(Integer rpms) {
        this.rpms = rpms;
    }




}
