/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
import java.util.Date;

public class OrdersDTO  implements java.io.Serializable {


     private int idOrders;
     private String userName;
     private Integer price;
     private Date date;

    public OrdersDTO() {
    }

	
    
    public OrdersDTO(int idOrders, String userName, Integer price, Date date) {
       this.idOrders = idOrders;
       this.userName = userName;
       this.price = price;
       this.date = date;
    }
   
    public int getIdOrders() {
        return this.idOrders;
    }
    
    public void setIdOrders(int idOrders) {
        this.idOrders = idOrders;
    }
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public Integer getPrice() {
        return this.price;
    }
    
    public void setPrice(Integer price) {
        this.price = price;
    }
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }




}

