/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class logInDTO {
    private String username;
    private String password;
    
    public logInDTO()
    {
    
    }
    
    public void setPassword(String password)
    {
        this.password=password;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setUsername(String username)
    {
        this.username=username;
    }
    
    public String getUsername()
    {
        return username;
    }
}
