/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */
public class UsersDTO  implements java.io.Serializable {


     private String userName;
     private String password;
     private String email;
     private Integer level;
     private String address;

    public UsersDTO() {
    }

    public UsersDTO(String userName, String password, String email, int level, String address) {
       this.userName = userName;
       this.password = password;
       this.email = email;
       this.level = level;
       this.address = address;
    }
   
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public Integer getLevel() {
        return this.level;
    }
    
    public void setLevel(Integer level) {
        this.level = level;
    }
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }




}
