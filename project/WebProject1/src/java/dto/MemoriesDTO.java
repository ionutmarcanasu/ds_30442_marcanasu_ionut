/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author Ionut
 */public class MemoriesDTO  implements java.io.Serializable {


     private int idMemories;
     private String manufacturer;
     private String model;
     private Integer ram;
     private Integer frequency;

    public MemoriesDTO() {
    }

	
    public MemoriesDTO(int idMemories, String manufacturer, String model, int ram, int frequency) {
       this.idMemories = idMemories;
       this.manufacturer = manufacturer;
       this.model = model;
       this.ram = ram;
       this.frequency = frequency;
    }
   
    public int getIdMemories() {
        return this.idMemories;
    }
    
    public void setIdMemories(int idMemories) {
        this.idMemories = idMemories;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getRam() {
        return this.ram;
    }
    
    public void setRam(Integer ram) {
        this.ram = ram;
    }
    public Integer getFrequency() {
        return this.frequency;
    }
    
    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

}
