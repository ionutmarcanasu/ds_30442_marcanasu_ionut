/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ro.tuc.dsrl.ds.handson.assig.one.server.servlets;

import org.hibernate.cfg.Configuration;
import ro.tuc.dsrl.ds.handson.assig.one.protocol.encoders.ResponseMessageEncoder;
import ro.tuc.dsrl.ds.handson.assig.one.protocol.enums.StatusCode;
import ro.tuc.dsrl.ds.handson.assig.one.protocol.message.RequestMessage;
import ro.tuc.dsrl.ds.handson.assig.one.protocol.servlets.AbstractServlet;
import ro.tuc.dsrl.ds.handson.assig.one.server.dao.StudentDAO;
import ro.tuc.dsrl.ds.handson.assig.one.server.entities.Student;

/**
 *
 * @author Ionut
 */
public class DeleteServlet extends AbstractServlet {
    private StudentDAO studentDao;

    public DeleteServlet() {
        studentDao = new StudentDAO(new Configuration().configure().buildSessionFactory());
    }
    
    @Override
    public String doGet(RequestMessage message) {
        String response;

        // Get from the query values the desired id
        String id = message.getQueryValues().get("id");
        // Find student in database and generate response
        if (id != null) {
            try {
                Student student = studentDao.findStudent(Integer.parseInt(id));

                if (student == null) {
                    response = ResponseMessageEncoder.encode(StatusCode.NOT_FOUND);
                } else {
                    response = ResponseMessageEncoder.encode(StatusCode.OK, student);
                }
            } catch (NumberFormatException e) {
                response = ResponseMessageEncoder.encode(StatusCode.BAD_REQUEST);
            }
        }
        // Id missing from request
        else {
            response = ResponseMessageEncoder.encode(StatusCode.BAD_REQUEST);
        }

        return response;
    }
    
     @Override
    public String doPost(RequestMessage message) {
         String response;
         String id="xl";
          id=message.getSerializedObject();
        //  id=message.
         
         if (id != null) {
            try {
                Student student = studentDao.findStudent(Integer.parseInt(id));
                
                
                if (student == null) {
                    response = ResponseMessageEncoder.encode(StatusCode.NOT_FOUND);
                } else {
                    studentDao.deleteStudent(student);
                    response = ResponseMessageEncoder.encode(StatusCode.OK, student);
                }
            } catch (NumberFormatException e) {
                response = ResponseMessageEncoder.encode(StatusCode.BAD_REQUEST);
            }
        }
        // Id missing from request
        else {
            response = ResponseMessageEncoder.encode(StatusCode.BAD_REQUEST);
        }
          return response;
    }
    
    @Override
    public String doDelete(RequestMessage message) {
         String response;
         String id="xl";
          id=message.getSerializedObject();
        //  id=message.
         
         if (id != null) {
            try {
                Student student = studentDao.findStudent(Integer.parseInt(id));
                
                
                if (student == null) {
                    response = ResponseMessageEncoder.encode(StatusCode.NOT_FOUND);
                } else {
                    studentDao.deleteStudent(student);
                    response = ResponseMessageEncoder.encode(StatusCode.OK, student);
                }
            } catch (NumberFormatException e) {
                response = ResponseMessageEncoder.encode(StatusCode.BAD_REQUEST);
            }
        }
        // Id missing from request
        else {
            response = ResponseMessageEncoder.encode(StatusCode.BAD_REQUEST);
        }
          return response;
    }
    
}
