/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class PackagesAccess {
     Session session =null;
    
    public PackagesAccess()
    {
      this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
    }
    
      public List getPackages()
    {
     
     List<Packages> packagesList = null;
    try {
         this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Packages as packages");
         packagesList = (List<Packages>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return  packagesList;
    }
    
       public void deletePackage(Packages pa)
       {
        try {
            this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.delete(pa);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
       
      public void insertPackage(Packages pa)
       {
         try {
            this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(pa);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
      
}
