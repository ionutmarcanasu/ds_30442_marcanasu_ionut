/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class UsersAccess {
    Session session =null;
    
    public UsersAccess()
    {
      this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
    }
    
    public List getUsers()
    {
     
     List<Users> usersList = null;
    try {
         this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Users as users ");
        usersList = (List<Users>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return usersList;
    }
    
    public void addUser(Users us)
    {
    try {
            this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(us);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
    }
    
     public void deleteUser(Users us)
       {
        try {
            this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.delete(us);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
      
}
