/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class RoutesAccess {
    Session session =null;
    
    public RoutesAccess()
    {
      this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
    }
    
     public List getRoutes()
    {
     
     List<Routes> routesList = null;
    try {
         this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Routes as routes");
         routesList = (List<Routes>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return  routesList;
    }
     
      public void deleteRoutes(Routes route)
       {
        try {
            this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.delete(route);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
      
      public void addRoutes(Routes route)
       {
        try {
            this.session = HibernateUtil1.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(route);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
     
}
