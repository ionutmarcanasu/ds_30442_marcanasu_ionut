/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package webServices;

import com.sun.xml.txw2.output.XmlSerializer;
import controllers.PackagesController;
import controllers.RoutesController;
import controllers.UsersController;
import dataAccess.Packages;
import dataAccess.PackagesAccess;
import dataAccess.Routes;
import dataAccess.RoutesAccess;
import dataAccess.Users;
import dataAccess.UsersAccess;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXB;
import listClasses.PackagesList;
import listClasses.RoutesList;
import listClasses.Userslist;

/**
 *
 * @author Ionut
 */
@WebService(serviceName = "NewWebService")
public class NewWebService {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
            
      // return "Hello " + txt + " !";
        //return getUsers();
      //  return getRoutes();
        return getPackages();
    }
    
     @WebMethod(operationName = "getUsers")
    public String getUsers()
    {
         UsersAccess usa=new UsersAccess();
            UsersController usc=new UsersController();
            Users us=usc.getUser(usa);
            List<Users> usl=usc.getUsers(usa);
            Userslist usersList=new Userslist();
            usersList.userl=(usl);
            
            return serializeToString(usersList);            
           
    }
    
    @WebMethod(operationName = "getRoutes")
    public String getRoutes()
    {
     RoutesAccess roa = new RoutesAccess();
     RoutesController roc=new RoutesController();
     List<Routes> rol=roc.getRoutes(roa);
     RoutesList routesList=new RoutesList();
     routesList.routesl=rol;
     
     return serializeToString(routesList);
    }
    
    @WebMethod(operationName = "getPackages")
    public String getPackages()
    {
     PackagesAccess paa = new PackagesAccess();
     PackagesController pac = new PackagesController();
     List<Packages> pal=pac.getPackages(paa);
     PackagesList packagesList=new PackagesList();
     packagesList.packagesl=pal;
     
     return serializeToString(packagesList);
    }
    
    @WebMethod(operationName="addRoute")
    public String AddRoute(int routeId,String packageName, String city, Date time)
    {
        Routes route=new Routes(routeId, packageName,city,time);       
        RoutesAccess roa=new RoutesAccess();
        RoutesController roc=new RoutesController();
        PackagesAccess paa=new PackagesAccess();
        PackagesController pac=new PackagesController();
        roc.addRoute(paa, roa, route);
        pac.setTrackingOn(paa, packageName);
        return "addedRoute";
    }
    
    @WebMethod(operationName = "deleteRoute")
    public String deleteRoute(int routeId)
    {
     RoutesAccess roa = new RoutesAccess();
     PackagesAccess paa= new PackagesAccess();
     RoutesController roc=new RoutesController();
     roc.deleteRoute(paa,roa, routeId);
     return "deletedRoute";
    }
    
     @WebMethod(operationName = "addPackage")
    public String addPackage(String name, String description, String senderClient, String recieverClient,String senderCity,String destinationCity)
    {
      Packages pack=new Packages(name,description,senderClient,recieverClient,senderCity,destinationCity,(byte)0);
      UsersAccess usa=new UsersAccess();
      PackagesAccess paa=new PackagesAccess();
      PackagesController pac=new PackagesController();
      pac.addPackage(pack, paa,usa);
      return "addedPackage";
    }
    
    @WebMethod(operationName = "deletePackage")
    public String deletePackage(String packageName)
    {
        RoutesAccess roa=new RoutesAccess();
        PackagesAccess paa= new PackagesAccess();
        RoutesController roc=new RoutesController();
        PackagesController pac=new PackagesController();
        pac.deletePackage(paa, packageName);
        List<Routes> routesList=roc.getRoutes(roa);
        for(Routes route : routesList)
        {
          if(route.getPackageName().equals(packageName))
          {   
              int routeId=route.getRouteId();
              roc.deleteRoute(paa, roa, routeId);
             }
        }
    
    return "deletedPackage";
    }
    
     @WebMethod(operationName = "deleteUser")
    public String deleteUser(String userName)
    {
         
        PackagesAccess paa= new PackagesAccess();
        UsersAccess usa=new UsersAccess();
        PackagesController pac=new PackagesController();
        UsersController usc=new UsersController();
        usc.deleteUser(usa, userName);
        List<Packages> packagesList=pac.getPackages(paa);
        for(Packages pa : packagesList)
        {
            if(pa.getRecieverClient().equals(userName)||pa.getSenderClient().equals(userName))
                deletePackage2(pa.getName());
        }
        
        return "userDeleted";
    }
    
    private void deletePackage2(String packageName)
    {
        RoutesAccess roa=new RoutesAccess();
        PackagesAccess paa= new PackagesAccess();
        RoutesController roc=new RoutesController();
        PackagesController pac=new PackagesController();
        pac.deletePackage(paa, packageName);
        List<Routes> routesList=roc.getRoutes(roa);
        for(Routes route : routesList)
        {
          if(route.getPackageName().equals(packageName))
          {   
              int routeId=route.getRouteId();
              roc.deleteRoute(paa, roa, routeId);
             }
        }
    }
    
    private String serializeToString(Object o)
    {
         StringWriter sw = new StringWriter();
          JAXB.marshal(o, sw);
          String xmlString=sw.toString();
           return xmlString;
    }
}
