/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listClasses;

import dataAccess.Users;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ionut
 */

@XmlRootElement(name ="UsersList")
public class Userslist implements java.io.Serializable {
   
     @XmlElementWrapper(name = "userl")
      @XmlElement(name = "Users")
    public List<Users> userl=null;
    public Userslist()
    {}
    
}
