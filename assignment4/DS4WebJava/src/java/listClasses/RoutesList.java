/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listClasses;

import dataAccess.Routes;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ionut
 */
@XmlRootElement(name ="RoutesList")
public class RoutesList implements java.io.Serializable {
   
     @XmlElementWrapper(name = "routesl")
      @XmlElement(name = "Routes")
    public List<Routes> routesl=null;
    public RoutesList()
    {}
    
}
