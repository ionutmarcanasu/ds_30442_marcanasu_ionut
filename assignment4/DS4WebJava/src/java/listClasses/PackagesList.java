/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listClasses;

import dataAccess.Packages;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ionut
 */
@XmlRootElement(name ="PackagesList")
public class PackagesList implements java.io.Serializable {
   
     @XmlElementWrapper(name = "packagesl")
      @XmlElement(name = "Packages")
    public List<Packages> packagesl=null;
    public PackagesList()
    {}
    
}