/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import dataAccess.Users;
import dataAccess.UsersAccess;
import java.util.List;

/**
 *
 * @author Ionut
 */
public class UsersController {
    
    public UsersController()
    {}
    
    public List<Users>  getUsers(UsersAccess usa)
    {
     return usa.getUsers();
    }
    
    public Users getUser(UsersAccess usa)
    {
      List<Users> usersList=usa.getUsers();
      return usersList.get(1);
    }
    
    public void deleteUser(UsersAccess usa, String userName)
    {
        List<Users> usersList=usa.getUsers();
        for(Users us : usersList)
        {
          if(us.getName().equals(userName))
          {
              usa.deleteUser(us);
              break;
          }
        }
    }
}
