/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import dataAccess.Packages;
import dataAccess.PackagesAccess;
import dataAccess.Routes;
import dataAccess.RoutesAccess;
import java.util.List;

/**
 *
 * @author Ionut
 */
public class RoutesController {
     public RoutesController()
    {}
    
    public List<Routes>  getRoutes(RoutesAccess roa)
    {
     return roa.getRoutes();
    }
    
    public void deleteRoute(PackagesAccess paa,RoutesAccess roa,int routeId)
    {
        String packageName="notfound";
      List<Routes> routesList=roa.getRoutes();
      for(Routes routes : routesList)
      {
          if(routes.getRouteId()==routeId)
          {   packageName=routes.getPackageName();
              roa.deleteRoutes(routes);
          }
      }
      if(!"notfound".equals(packageName))
      { int ok=0;
       List<Routes> routesListSearch = roa.getRoutes();
            for(Routes routes : routesListSearch)
            {
             if(routes.getPackageName().equals(packageName))
                 ok=1;
            }
           if(ok==0)
           {
               List<Packages> packagesList=paa.getPackages();
               for(Packages pack : packagesList)
               {
                   if(pack.getName().equals(packageName))
                   {
                    pack.setTracking((byte)0);
                    paa.insertPackage(pack);
                   }
               }
           }
       
      }
    } // end of deleteRoute !
    
    public void addRoute(PackagesAccess paa,RoutesAccess roa,Routes route)
    {
      List<Packages> packageList=paa.getPackages();
      for(Packages pack : packageList)
      {
       if(pack.getName().equals(route.getPackageName()))
       {
           roa.addRoutes(route);
           break;
       }
      }
    }
    
}
