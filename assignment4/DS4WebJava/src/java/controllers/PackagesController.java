/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import dataAccess.Packages;
import dataAccess.PackagesAccess;
import dataAccess.Users;
import dataAccess.UsersAccess;
import java.util.List;

/**
 *
 * @author Ionut
 */
public class PackagesController {
    
    public PackagesController()
    {}
    
    public List<Packages> getPackages(PackagesAccess paa)
    {
     return paa.getPackages();
    }
    
    public void addPackage(Packages pa,PackagesAccess paa,UsersAccess usa)
    {
        int a=0,b=0;
        
        List<Users> usersList=usa.getUsers();
        
        for(Users us1 : usersList)
        {
          if(us1.getName().equals(pa.getSenderClient()))
              a=1;
          if(us1.getName().equals(pa.getRecieverClient()))
              b=1;
        }
        
       if((a==1)&&(b==1)) 
                
      paa.insertPackage(pa);
    }
    
    public void setTrackingOn(PackagesAccess paa,String packageName)
    {
      List<Packages> pal=paa.getPackages();
      for(Packages pa : pal)
      {
       if(packageName.equals(pa.getName()))
       {
         pa.setTracking((byte) 1);
         paa.insertPackage(pa);
         break;
       }
      }
    }
    
    public void deletePackage(PackagesAccess paa, String packageName)
    {
        List<Packages> packagesList=paa.getPackages();
        for(Packages pack : packagesList)
        {
            if(pack.getName().equals(packageName))
            {
                paa.deletePackage(pack);
                break;
            }
        }
    }
}
