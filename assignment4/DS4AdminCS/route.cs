//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DS4AdminCS
{
    using System;
    using System.Collections.Generic;
    
    public partial class route
    {
        public string packageName { get; set; }
        public string city { get; set; }
        public Nullable<System.TimeSpan> time { get; set; }
        public int routeId { get; set; }
    }
}
