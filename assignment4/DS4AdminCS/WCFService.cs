﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ServiceModel;


namespace DS4AdminCS
{
     [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class WCFService : IServiceWCF
    {
       private Controller cont=new Controller();

       public WCFService()
       { }

       public string getPackages(string clientName)
       {
           string result = serializeToString(cont.getPackages(clientName));
           return result;  
       }

        public int checkUser(string username, string password)
      {
          return cont.checkUser(username,password);
       }


        public string getRoutes(string clientName)
        {

            string result = serializeToString(cont.getR2(clientName));
            return result;
        }

        public int addUser(string userName, string password)
        {
            return cont.addUser(userName,password);
        }

        private string serializeToString(Object obj)
        {

            XmlSerializer serializer = new XmlSerializer(obj.GetType());

            StringWriter writer = new StringWriter();

            serializer.Serialize(writer, obj);

            return writer.ToString();
        }

    }
}
