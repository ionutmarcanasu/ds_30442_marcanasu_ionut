﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DS4AdminCS
{
  public  class Controller
    {
      private DataAccess dataAccess = new DataAccess();

      public Controller()
      { }

      public int checkUser(String username, String password)
      {
          List<user> userList = dataAccess.getUsers();
          foreach (user us in userList)
          {
              if (us.name == username)
                  if (us.password == password)
                  {
                      int lv =(int) us.level;
                      return lv;
                  }
          }

          return 0;
      }

      public List<package> getPackages(String clientName)
      {
          List<package> packageList = new List<package>();
          foreach (package pack in dataAccess.getPackages())
          {
              if (pack.recieverClient == clientName || pack.senderClient == clientName)
                  packageList.Add(pack);
          }
          return packageList;     
      }

      private List<route> getRoutes(String clientName)
      {
          List<route> routesList = new List<route>();
          List<package> packageList = getPackages(clientName);
          foreach(package pack in packageList)
              foreach (route rou in dataAccess.getRoutes())
              {
                  if (pack.name == rou.packageName)
                      routesList.Add(rou);
              }
          return routesList;
      }

      public List<routes2> getR2(String clientName)
      {
          List<routes2> routes2list = routeListConv(getRoutes(clientName));
          return routes2list;
      }

      private List<routes2> routeListConv(List<route> routesList)
      {
          List<routes2> routes2list = new List<routes2>();
          foreach (route rou in routesList)
          {

              string time2 = rou.time.Value.Hours + ":" + rou.time.Value.Minutes + ":" + rou.time.Value.Seconds;
              //  string time2 = rou.time.Value.Hour + ":" + rou.time.Value.Minute + ":" + rou.time.Value.Second;
              routes2 rou2 = new routes2(rou.packageName, rou.city, time2, rou.routeId);
              routes2list.Add(rou2);
          }
          return routes2list;
      }

      public int addUser(string userName, string password)
      { 
          int ok=1;
          foreach (user us in dataAccess.getUsers())
          {
              if (us.name == userName)
              {
                  ok = 0;
                  break;
              }
          }
          if (ok == 1)
          {
              dataAccess.addUser(userName,password);
          }

          return ok;
      }
    

    }
}
