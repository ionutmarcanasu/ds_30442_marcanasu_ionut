﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DS4AdminCS
{
    class Program
    {
        private static ServiceHost host;

        static void Main(string[] args)
        {
            /*
            WCFService wcfservice = new WCFService();
            int lv =wcfservice.checkUser("Mary","0000");
            string res = wcfservice.getPackages("Mary");
            string res2 = wcfservice.getRoutes("Mary");
            int add = wcfservice.addUser("Sarah","911");
            System.Console.WriteLine("result : "+lv);
            System.Console.WriteLine(res);
            System.Console.WriteLine(res2);
            System.Console.WriteLine(add);
             * */
            InitializeService();
            host.Open();
            System.Console.WriteLine("WCF DS4 Service running...");
            System.Console.ReadLine();
            host.Close();

        }

        private static void InitializeService()
        {
            host = new ServiceHost(
                    typeof(WCFService),
                    new Uri[]{
                    new Uri("net.tcp://localhost"  + ":8021/Service1"),
                    });

            NetTcpBinding netTcp = new NetTcpBinding();
            netTcp.Security.Mode = SecurityMode.None;
            host.AddServiceEndpoint(typeof(IServiceWCF),
              netTcp,
              "DS4Service");
        }

    }
}
