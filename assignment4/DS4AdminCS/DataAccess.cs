﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace DS4AdminCS
{
    public class DataAccess
    {
        private ds4Entities ds4Context = new ds4Entities();

        public DataAccess()
        { }


        public List<package> getPackages()
        {
           
          List<package> packageList = ds4Context.packages.ToList();
          return packageList;
        }

        public List<route> getRoutes()
        {
            List<route> routeList = ds4Context.routes.ToList();
            return routeList;
        }

        public List<user> getUsers()
        {
            List<user> userList = ds4Context.users.ToList();
            return userList;
        }

        public void addUser(string username,string password)
        {
            user us = new user();
            us.name = username;
            us.password = password;
            us.level = 1;
            ds4Context.users.Add(us);
            ds4Context.SaveChanges();
        }
    }
}
