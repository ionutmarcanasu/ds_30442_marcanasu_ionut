﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4AdminCS
{
     [Serializable]
  public  class routes2
    {
     
        public string packageName { get; set; }
        public string city { get; set; }
        public string time { get; set; }
        //  public Nullable<System.DateTime> time { get; set; }
        public int routeId { get; set; }

        public routes2()
        { }

        public routes2(string packageName, string city, string time, int routeid)
        {
            this.packageName = packageName;
            this.city = city;
            this.time = time;
            this.routeId = routeid;
        }
    }
}
