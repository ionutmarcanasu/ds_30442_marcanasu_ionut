﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace DS4AdminCS
{
    [ServiceContract]
    public interface IServiceWCF
    {
         [OperationContract]
        string getPackages(string clientName);
         [OperationContract]
        int checkUser(string username, string password);
         [OperationContract]
        string getRoutes(string clientName);
         [OperationContract]
        int addUser(string userName, string password);

    }
}
