﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DS4GUI.EntityClasses;
using System.ServiceModel;
using System.Xml.Serialization;
using System.IO;

namespace DS4GUI
{
    public partial class ClientForm : Form
    {
        string clientName;
        IServiceWCF serviceProvider;

        public ClientForm()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        public ClientForm(string clientName, IServiceWCF serviceProvider)
        {
            this.clientName = clientName;
            this.serviceProvider = serviceProvider;
            InitializeComponent();
            loadClient();
            this.CenterToScreen();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            loadClient();
            textBox1.Clear();
        }

        private void buttonSearchPackage_Click(object sender, EventArgs e)
        {
            int found = 0;
            string search = textBox1.Text;
            string packages = serviceProvider.getPackages(clientName);
            List<package> packageList = (List<package>)deserializeString(packages, typeof(List<package>));
            foreach (package pac in packageList)
                if (pac.name == search)
                {
                    List<package> packl2 = new List<package>();
                    packl2.Add(pac);
                    dataGridView1.DataSource = packl2;
                    found = 1;
                    break;
                }
            if (found == 1)
            {
                string routes = serviceProvider.getRoutes(clientName);
                List<routes2> returnList=(List<routes2>)deserializeString(routes,typeof(List<routes2>));
                List<routes2> routesList=new List<routes2>();
                foreach (routes2 rou in returnList)
                {
                    if (rou.packageName == search)
                        routesList.Add(rou);
                }
                dataGridView2.DataSource = routesList;
            }
        }

        private void loadClient()
        {   string packages=serviceProvider.getPackages(clientName);
            List<package> packageList=(List<package>)deserializeString(packages,typeof (List<package>));
            dataGridView1.DataSource = packageList;
            string routes = serviceProvider.getRoutes(clientName);
            if (routes != null)
            {
                List<routes2> routeList = (List<routes2>)deserializeString(routes, typeof(List<routes2>));
                
                dataGridView2.DataSource = routeList;
             
            }
        }

        private Object deserializeString(string data, Type tp)
        {
            var serializer = new XmlSerializer(tp);
            using (TextReader reader = new StringReader(data))
            {
                return serializer.Deserialize(reader);
            }
        }

       
    }
}
