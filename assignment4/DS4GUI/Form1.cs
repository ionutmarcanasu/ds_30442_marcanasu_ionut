﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using DS4GUI.ServiceReference1;
using DS4GUI.BasicClasses;
using System.IO;
using System.ServiceModel;


namespace DS4GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
            ServerConnect();
        }

        IServiceWCF serviceProvider;
        ChannelFactory<IServiceWCF> netTcpFactory;

        private void button1_Click(object sender, EventArgs e)
        {
            string userName;
            string pass;
            int level=0;
            try
            {
                userName = textBox1.Text;
                pass = textBox2.Text;
                level = serviceProvider.checkUser(userName,pass);
            }
            catch(Exception ex)
            { textBox1.AppendText("login error!");
            return;
            }
            if (level == 0)
            {
                textBox1.AppendText("invalid user/password");
                return;
            }
            if (level == 2)
            {
                AdminForm frm = new AdminForm();
                frm.Show();
            }
            if (level == 1)
            {
               ClientForm frm = new ClientForm(userName,serviceProvider);
                frm.Show();
            }
        }


       

        private void button2_Click(object sender, EventArgs e)
        {
            string userName;
            string pass;
            int result = 5;
            try
            {
                userName = textBox1.Text;
                pass = textBox2.Text;
                result = serviceProvider.addUser(userName,pass);
            }
            catch (Exception ex)
            {
                textBox1.AppendText("register error!");
                return;
            }
            if (result == 0)
            {
                textBox1.AppendText("username already taken");
            }
            if (result == 1)
            {
                textBox1.AppendText("successful");
                textBox2.AppendText("registration");
            }
        }

       private void ServerConnect()
        {
            NetTcpBinding netTcp = new NetTcpBinding();
           
            netTcp.Security.Mode = SecurityMode.None;
            netTcpFactory = new ChannelFactory<IServiceWCF>(netTcp, new EndpointAddress(
                  "net.tcp://localhost:8021/Service1/DS4Service"));

            serviceProvider = netTcpFactory.CreateChannel();
        }


        /*
       private string serializeToString(Object obj)
       {

           XmlSerializer serializer = new XmlSerializer(obj.GetType());

           StringWriter writer = new StringWriter();

           serializer.Serialize(writer, obj);

           return writer.ToString();
       }


       private Object deserializeFromJava(string data, Type tp)
       {
           var serializer = new XmlSerializer(tp);
           using (TextReader reader = new StringReader(data))
           {
               return serializer.Deserialize(reader);
           }
       }
         * */
    }
}
