﻿namespace DS4GUI
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewUsers = new System.Windows.Forms.DataGridView();
            this.buttonUserDelete = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewPackages = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridViewRoutes = new System.Windows.Forms.DataGridView();
            this.insertPackagesButton = new System.Windows.Forms.Button();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.textBoxSenderClient = new System.Windows.Forms.TextBox();
            this.textBoxRecieverClient = new System.Windows.Forms.TextBox();
            this.textBoxSenderCity = new System.Windows.Forms.TextBox();
            this.textBoxDestinationCity = new System.Windows.Forms.TextBox();
            this.textboxPackageName = new System.Windows.Forms.TextBox();
            this.textBoxDeletePackage = new System.Windows.Forms.TextBox();
            this.buttonDeletePackage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonInsertRoutes = new System.Windows.Forms.Button();
            this.textBoxRoutesId = new System.Windows.Forms.TextBox();
            this.textBoxRoutesPackageName = new System.Windows.Forms.TextBox();
            this.textBoxRoutesCity = new System.Windows.Forms.TextBox();
            this.textBoxRoutesTime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRoutesDeleteId = new System.Windows.Forms.TextBox();
            this.buttonRoutesDelete = new System.Windows.Forms.Button();
            this.getUsersResponseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxUsersNameDelete = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPackages)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getUsersResponseBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(784, 430);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBoxUsersNameDelete);
            this.tabPage1.Controls.Add(this.dataGridViewUsers);
            this.tabPage1.Controls.Add(this.buttonUserDelete);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(776, 404);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "UsersTab";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewUsers
            // 
            this.dataGridViewUsers.AllowUserToAddRows = false;
            this.dataGridViewUsers.AllowUserToDeleteRows = false;
            this.dataGridViewUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUsers.Location = new System.Drawing.Point(7, 7);
            this.dataGridViewUsers.Name = "dataGridViewUsers";
            this.dataGridViewUsers.ReadOnly = true;
            this.dataGridViewUsers.Size = new System.Drawing.Size(763, 190);
            this.dataGridViewUsers.TabIndex = 2;
            // 
            // buttonUserDelete
            // 
            this.buttonUserDelete.Location = new System.Drawing.Point(328, 350);
            this.buttonUserDelete.Name = "buttonUserDelete";
            this.buttonUserDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonUserDelete.TabIndex = 1;
            this.buttonUserDelete.Text = "Delete";
            this.buttonUserDelete.UseVisualStyleBackColor = true;
            this.buttonUserDelete.Click += new System.EventHandler(this.buttonUserDelete_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.buttonDeletePackage);
            this.tabPage2.Controls.Add(this.textBoxDeletePackage);
            this.tabPage2.Controls.Add(this.textboxPackageName);
            this.tabPage2.Controls.Add(this.textBoxDestinationCity);
            this.tabPage2.Controls.Add(this.textBoxSenderCity);
            this.tabPage2.Controls.Add(this.textBoxRecieverClient);
            this.tabPage2.Controls.Add(this.textBoxSenderClient);
            this.tabPage2.Controls.Add(this.textBoxDescription);
            this.tabPage2.Controls.Add(this.insertPackagesButton);
            this.tabPage2.Controls.Add(this.dataGridViewPackages);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(776, 404);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "PackagesTab";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPackages
            // 
            this.dataGridViewPackages.AllowUserToAddRows = false;
            this.dataGridViewPackages.AllowUserToDeleteRows = false;
            this.dataGridViewPackages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPackages.Location = new System.Drawing.Point(4, 7);
            this.dataGridViewPackages.Name = "dataGridViewPackages";
            this.dataGridViewPackages.ReadOnly = true;
            this.dataGridViewPackages.Size = new System.Drawing.Size(766, 176);
            this.dataGridViewPackages.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonRoutesDelete);
            this.tabPage3.Controls.Add(this.textBoxRoutesDeleteId);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.textBoxRoutesTime);
            this.tabPage3.Controls.Add(this.textBoxRoutesCity);
            this.tabPage3.Controls.Add(this.textBoxRoutesPackageName);
            this.tabPage3.Controls.Add(this.textBoxRoutesId);
            this.tabPage3.Controls.Add(this.buttonInsertRoutes);
            this.tabPage3.Controls.Add(this.dataGridViewRoutes);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(776, 404);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "RoutesTab";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRoutes
            // 
            this.dataGridViewRoutes.AllowUserToAddRows = false;
            this.dataGridViewRoutes.AllowUserToDeleteRows = false;
            this.dataGridViewRoutes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRoutes.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewRoutes.Name = "dataGridViewRoutes";
            this.dataGridViewRoutes.ReadOnly = true;
            this.dataGridViewRoutes.Size = new System.Drawing.Size(767, 159);
            this.dataGridViewRoutes.TabIndex = 0;
            // 
            // insertPackagesButton
            // 
            this.insertPackagesButton.Location = new System.Drawing.Point(309, 281);
            this.insertPackagesButton.Name = "insertPackagesButton";
            this.insertPackagesButton.Size = new System.Drawing.Size(75, 23);
            this.insertPackagesButton.TabIndex = 1;
            this.insertPackagesButton.Text = "Insert";
            this.insertPackagesButton.UseVisualStyleBackColor = true;
            this.insertPackagesButton.Click += new System.EventHandler(this.insertPackagesButton_Click);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(151, 226);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(100, 20);
            this.textBoxDescription.TabIndex = 2;
            // 
            // textBoxSenderClient
            // 
            this.textBoxSenderClient.Location = new System.Drawing.Point(274, 226);
            this.textBoxSenderClient.Name = "textBoxSenderClient";
            this.textBoxSenderClient.Size = new System.Drawing.Size(100, 20);
            this.textBoxSenderClient.TabIndex = 3;
            // 
            // textBoxRecieverClient
            // 
            this.textBoxRecieverClient.Location = new System.Drawing.Point(400, 226);
            this.textBoxRecieverClient.Name = "textBoxRecieverClient";
            this.textBoxRecieverClient.Size = new System.Drawing.Size(100, 20);
            this.textBoxRecieverClient.TabIndex = 4;
            // 
            // textBoxSenderCity
            // 
            this.textBoxSenderCity.Location = new System.Drawing.Point(523, 226);
            this.textBoxSenderCity.Name = "textBoxSenderCity";
            this.textBoxSenderCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxSenderCity.TabIndex = 5;
            // 
            // textBoxDestinationCity
            // 
            this.textBoxDestinationCity.Location = new System.Drawing.Point(651, 226);
            this.textBoxDestinationCity.Name = "textBoxDestinationCity";
            this.textBoxDestinationCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxDestinationCity.TabIndex = 6;
            // 
            // textboxPackageName
            // 
            this.textboxPackageName.Location = new System.Drawing.Point(24, 226);
            this.textboxPackageName.Name = "textboxPackageName";
            this.textboxPackageName.Size = new System.Drawing.Size(100, 20);
            this.textboxPackageName.TabIndex = 7;
            // 
            // textBoxDeletePackage
            // 
            this.textBoxDeletePackage.Location = new System.Drawing.Point(603, 313);
            this.textBoxDeletePackage.Name = "textBoxDeletePackage";
            this.textBoxDeletePackage.Size = new System.Drawing.Size(100, 20);
            this.textBoxDeletePackage.TabIndex = 8;
            // 
            // buttonDeletePackage
            // 
            this.buttonDeletePackage.Location = new System.Drawing.Point(621, 349);
            this.buttonDeletePackage.Name = "buttonDeletePackage";
            this.buttonDeletePackage.Size = new System.Drawing.Size(75, 23);
            this.buttonDeletePackage.TabIndex = 9;
            this.buttonDeletePackage.Text = "Delete";
            this.buttonDeletePackage.UseVisualStyleBackColor = true;
            this.buttonDeletePackage.Click += new System.EventHandler(this.buttonDeletePackage_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(600, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "PackageNameToDelete";
            // 
            // buttonInsertRoutes
            // 
            this.buttonInsertRoutes.Location = new System.Drawing.Point(301, 272);
            this.buttonInsertRoutes.Name = "buttonInsertRoutes";
            this.buttonInsertRoutes.Size = new System.Drawing.Size(75, 23);
            this.buttonInsertRoutes.TabIndex = 1;
            this.buttonInsertRoutes.Text = "Insert";
            this.buttonInsertRoutes.UseVisualStyleBackColor = true;
            this.buttonInsertRoutes.Click += new System.EventHandler(this.buttonInsertRoutes_Click);
            // 
            // textBoxRoutesId
            // 
            this.textBoxRoutesId.Location = new System.Drawing.Point(64, 215);
            this.textBoxRoutesId.Name = "textBoxRoutesId";
            this.textBoxRoutesId.Size = new System.Drawing.Size(100, 20);
            this.textBoxRoutesId.TabIndex = 2;
            // 
            // textBoxRoutesPackageName
            // 
            this.textBoxRoutesPackageName.Location = new System.Drawing.Point(228, 215);
            this.textBoxRoutesPackageName.Name = "textBoxRoutesPackageName";
            this.textBoxRoutesPackageName.Size = new System.Drawing.Size(100, 20);
            this.textBoxRoutesPackageName.TabIndex = 3;
            // 
            // textBoxRoutesCity
            // 
            this.textBoxRoutesCity.Location = new System.Drawing.Point(382, 215);
            this.textBoxRoutesCity.Name = "textBoxRoutesCity";
            this.textBoxRoutesCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxRoutesCity.TabIndex = 4;
            // 
            // textBoxRoutesTime
            // 
            this.textBoxRoutesTime.Location = new System.Drawing.Point(552, 215);
            this.textBoxRoutesTime.Name = "textBoxRoutesTime";
            this.textBoxRoutesTime.Size = new System.Drawing.Size(100, 20);
            this.textBoxRoutesTime.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(620, 302);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "RouteIdToDelete";
            // 
            // textBoxRoutesDeleteId
            // 
            this.textBoxRoutesDeleteId.Location = new System.Drawing.Point(623, 319);
            this.textBoxRoutesDeleteId.Name = "textBoxRoutesDeleteId";
            this.textBoxRoutesDeleteId.Size = new System.Drawing.Size(100, 20);
            this.textBoxRoutesDeleteId.TabIndex = 7;
            // 
            // buttonRoutesDelete
            // 
            this.buttonRoutesDelete.Location = new System.Drawing.Point(623, 345);
            this.buttonRoutesDelete.Name = "buttonRoutesDelete";
            this.buttonRoutesDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonRoutesDelete.TabIndex = 8;
            this.buttonRoutesDelete.Text = "Delete";
            this.buttonRoutesDelete.UseVisualStyleBackColor = true;
            this.buttonRoutesDelete.Click += new System.EventHandler(this.buttonRoutesDelete_Click);
            // 
            // getUsersResponseBindingSource
            // 
            this.getUsersResponseBindingSource.DataSource = typeof(DS4GUI.ServiceReference1.getUsersResponse);
            // 
            // textBoxUsersNameDelete
            // 
            this.textBoxUsersNameDelete.Location = new System.Drawing.Point(328, 324);
            this.textBoxUsersNameDelete.Name = "textBoxUsersNameDelete";
            this.textBoxUsersNameDelete.Size = new System.Drawing.Size(100, 20);
            this.textBoxUsersNameDelete.TabIndex = 3;
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 436);
            this.Controls.Add(this.tabControl1);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPackages)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getUsersResponseBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonUserDelete;
        private System.Windows.Forms.BindingSource getUsersResponseBindingSource;
        private System.Windows.Forms.DataGridView dataGridViewUsers;
        private System.Windows.Forms.DataGridView dataGridViewPackages;
        private System.Windows.Forms.DataGridView dataGridViewRoutes;
        private System.Windows.Forms.TextBox textboxPackageName;
        private System.Windows.Forms.TextBox textBoxDestinationCity;
        private System.Windows.Forms.TextBox textBoxSenderCity;
        private System.Windows.Forms.TextBox textBoxRecieverClient;
        private System.Windows.Forms.TextBox textBoxSenderClient;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button insertPackagesButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDeletePackage;
        private System.Windows.Forms.TextBox textBoxDeletePackage;
        private System.Windows.Forms.Button buttonRoutesDelete;
        private System.Windows.Forms.TextBox textBoxRoutesDeleteId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRoutesTime;
        private System.Windows.Forms.TextBox textBoxRoutesCity;
        private System.Windows.Forms.TextBox textBoxRoutesPackageName;
        private System.Windows.Forms.TextBox textBoxRoutesId;
        private System.Windows.Forms.Button buttonInsertRoutes;
        private System.Windows.Forms.TextBox textBoxUsersNameDelete;
    }
}