﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.EntityClasses
{
    public partial class user
    {
        public string name { get; set; }
        public string password { get; set; }
        public Nullable<int> level { get; set; }
    }
}
