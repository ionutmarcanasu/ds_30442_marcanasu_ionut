﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.EntityClasses
{
    public partial class package
    {
        public string name { get; set; }
        public string description { get; set; }
        public string senderClient { get; set; }
        public string recieverClient { get; set; }
        public string senderCity { get; set; }
        public string destinationCity { get; set; }
        public sbyte tracking { get; set; }
    }
}
