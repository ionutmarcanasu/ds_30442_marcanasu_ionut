﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using DS4GUI.ServiceReference1;
using DS4GUI.BasicClasses;

namespace DS4GUI
{
    public partial class AdminForm : Form
    {
        NewWebServiceClient proxyAdmin;

        public AdminForm()
        {
            InitializeComponent();
            this.CenterToScreen();
            proxyAdmin = new NewWebServiceClient();
            init2();
            
        }

        private void init2()
        {
            string usersResponse = proxyAdmin.getUsers();
            UsersList usl = (UsersList)deserializeFromJava(usersResponse,typeof(UsersList));
            //Users us1 = usl.getUsers().ElementAt(1);

            string packagesResponse=proxyAdmin.getPackages();
            PackagesList pal=(PackagesList)deserializeFromJava(packagesResponse,typeof(PackagesList));

            string routesResponse=proxyAdmin.getRoutes();
            RoutesList rol=(RoutesList)deserializeFromJava(routesResponse,typeof(RoutesList));

            dataGridViewUsers.DataSource = usl.getUsers();
            dataGridViewPackages.DataSource = pal.getPackages();
            List<routes2> rol2 = routeListConv(rol.getRoutes());
            dataGridViewRoutes.DataSource = rol2;
              
        }

        

        private void buttonUserDelete_Click(object sender, EventArgs e)
        {
            string userName = textBoxUsersNameDelete.Text;
            string result = proxyAdmin.deleteUser(userName);
            init2();
        }



        private Object deserializeFromJava(string data, Type tp)
        {
            var serializer = new XmlSerializer(tp);
            using (TextReader reader = new StringReader(data))
            {
                return serializer.Deserialize(reader);
            }
        }

        private void insertPackagesButton_Click(object sender, EventArgs e)
        {
            try
            {
                string packagename = textboxPackageName.Text;
                string description = textBoxDescription.Text;
                string senderClient = textBoxSenderClient.Text;
                string recieverClient = textBoxRecieverClient.Text;
                string senderCity = textBoxSenderCity.Text;
                string destination = textBoxDestinationCity.Text;
               string result= proxyAdmin.addPackage(packagename, description, senderClient, recieverClient, senderCity, destination);
            }
            catch (FormatException ex)
            { }
            finally { init2();
                      // this.Refresh();
                     }
        }

        private void buttonInsertRoutes_Click(object sender, EventArgs e)
        {

            try
            {
                int routeId = Int16.Parse(textBoxRoutesId.Text);
                string packageName = textBoxRoutesPackageName.Text;
                string city = textBoxRoutesCity.Text;
                DateTime datetime1 = DateTime.Parse(textBoxRoutesTime.Text);
                string result = proxyAdmin.addRoute(routeId,packageName,city,datetime1);
            }
            catch(FormatException ex)
            {}
            finally{
                    init2();
                }
        }

        private void buttonRoutesDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int routeIdDelete = Int16.Parse(textBoxRoutesDeleteId.Text);
                string result=proxyAdmin.deleteRoute(routeIdDelete);
            }
            catch (FormatException ex)
            { }
            finally
            {
                init2();
            }
        }

        private void buttonDeletePackage_Click(object sender, EventArgs e)
        {
            string packageName = textBoxDeletePackage.Text;
            proxyAdmin.deletePackage(packageName);
            init2();
        }


        private List<routes2> routeListConv(List<Routes> routesList)
        {
            List<routes2> routes2list=new List<routes2>();
            foreach(Routes rou in routesList)
            {
              string time2=rou.time.Hour+":"+rou.time.Minute+":"+rou.time.Second;
              routes2 rou2 = new routes2(rou.packageName,rou.city,time2,rou.routeId);
              routes2list.Add(rou2);
            }
            return routes2list;
        }
    }
}
