﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DS4GUI
{
    public class SerializationHelper
    {

        public string serialize(object obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                XmlSerializer xs = new XmlSerializer(obj.GetType());
                xs.Serialize(stream, obj);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        public object deserialize(string serialized, Type type)
        {
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(serialized)))
            {
                using (var reader = new PascalCaseTransfomingReader(stream))
                {
                    XmlSerializer xs = new XmlSerializer(type);
                    return xs.Deserialize(reader);
                }
            }
        }

        private class PascalCaseTransfomingReader : XmlTextReader
        {
            public PascalCaseTransfomingReader(Stream input) : base(input) { }

            public override string this[string name]
            {
                get { return this[name, String.Empty]; }
            }

            public override string LocalName
            {
                get
                {
                    // Capitalize first letter of elements and attributes.
                    if (base.NodeType == XmlNodeType.Element ||
                        base.NodeType == XmlNodeType.EndElement ||
                        base.NodeType == XmlNodeType.Attribute)
                    {
                        return base.NamespaceURI == "http://www.w3.org/2000/xmlns/" ?
                               base.LocalName : MakeFirstUpper(base.LocalName);
                    }
                    return base.LocalName;
                }
            }

            public override string Name
            {
                get
                {
                    if (base.NamespaceURI == "http://www.w3.org/2000/xmlns/")
                        return base.Name;
                    if (base.Name.IndexOf(":") == -1)
                        return MakeFirstUpper(base.Name);
                    else
                    {
                        // Turn local name into upper, not the prefix.
                        string name = base.Name.Substring(0, base.Name.IndexOf(":") + 1);
                        name += MakeFirstUpper(base.Name.Substring(base.Name.IndexOf(":") + 1));
                        return NameTable.Add(name);
                    }
                }
            }

            private string MakeFirstUpper(string name)
            {
                if (name.Length == 0) return name;
                if (Char.IsUpper(name[0])) return name;
                if (name.Length == 1) return name.ToUpper();
                Char[] letters = name.ToCharArray();
                letters[0] = Char.ToUpper(letters[0]);
                return NameTable.Add(new string(letters));
            }

        }
    }
}
