﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.BasicClasses
{
    [Serializable]
  public class Users
    {
        public string name { get; set; }
        public string password { get; set; }
        public int level { get; set; }

       private Users()
        { }

        public Users(string name , string password , int level)
        {
            this.name = name;
            this.password = password;
            this.level = level;
        }      
        
    }
}
