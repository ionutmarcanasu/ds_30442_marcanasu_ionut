﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.BasicClasses
{
    [Serializable]
   public class PackagesList
    {
        public List<Packages> packagesl;

       public PackagesList()
       { packagesl=new List<Packages>(); }

       public List<Packages> getPackages()
       {
           return packagesl;
       }

       public void setPackages(List<Packages> pal)
       {
           this.packagesl = pal;
       }

    }
}
