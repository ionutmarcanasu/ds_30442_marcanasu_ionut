﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.BasicClasses
{
    [Serializable]
   public class UsersList
    {

       public List<Users> userl;

       public UsersList()
       { userl=new List<Users>(); }

       public List<Users> getUsers()
       {
           return userl;
       }

       public void setUsers(List<Users> usl)
       {
           this.userl = usl;
       }
    }
}
