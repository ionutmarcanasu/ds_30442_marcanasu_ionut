﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.BasicClasses
{
    [Serializable]
   public class Routes
    {
        public int routeId { get; set; }
        public string packageName { get; set; }
        public string city { get; set; }
        public DateTime time { get; set; }

        private Routes()
        { }

        public Routes(int routeId, string packageName, string city,DateTime time)
        {
            this.routeId = routeId;
            this.packageName = packageName;
            this.city = city;
            this.time = time;
        }


    }
}
