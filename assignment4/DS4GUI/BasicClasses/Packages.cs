﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS4GUI.BasicClasses
{
    [Serializable]
   public class Packages
    {
        public string name { get; set; }
        public string description { get; set; }
        public string senderClient { get; set; }
        public string recieverClient { get; set; }
        public string senderCity { get; set; }
        public string destinationCity { get; set; }
        public byte tracking { get; set; }

        private Packages()
        { }

        public Packages(string name , string description , string senderClient , string recieverClient , string senderCity , string destinationCity , byte tracking)
        {
            this.name = name;
            this.description = description;
            this.senderClient = senderClient;
            this.recieverClient = recieverClient;
            this.senderCity = senderCity;
            this.destinationCity = destinationCity;
            this.tracking = tracking;
        }

    }
}
