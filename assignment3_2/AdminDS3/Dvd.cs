﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminDS3
{
    class Dvd
    {
      private  string title;
      private  int year;
      private  double price;

      public Dvd(string title, int year, double price)
      {
          this.title = title;
          this.year = year;
          this.price = price;
      }

        public string toString()
        {
            return " title: " + title + " year: " + year + " price: "+price;
        }
    }
}
