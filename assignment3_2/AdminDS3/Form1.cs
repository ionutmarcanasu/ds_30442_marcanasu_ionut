﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RabbitMQ.Client;


namespace AdminDS3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello", durable: false, exclusive: false, autoDelete: false, arguments: null);

                string message = "Hello Worldd!";
                message = parseText();
                if (message != "error")
                {
                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "", routingKey: "hello", basicProperties: null, body: body);
                    // Console.WriteLine(" [x] Sent {0}", message);
                   //  textBox1.AppendText("Sent: " + message);
                }
                else
                  {
                      textBox1.Text = "Input";
                      textBox2.Text = "Error";
                      textBox3.Text = "Detected";
                  }
            }
        }
         */

        private void button1_Click(object sender, EventArgs e)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "logs", type: "fanout");

                var message = parseText();
                if (message != "error")
                {
                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "logs", routingKey: "", basicProperties: null, body: body);
                }
                else
                {
                    textBox1.Text = "Input";
                    textBox2.Text = "Error";
                    textBox3.Text = "Detected";
                }
                //Console.WriteLine(" [x] Sent {0}", message);
            }

        }

        private string parseText()
        {
            try
            {
                string title = textBox1.Text;
                int year = Int32.Parse(textBox2.Text);
                double price = double.Parse(textBox3.Text);
                Dvd dvd1 = new Dvd(title, year, price);
                return dvd1.toString();
            }
            catch (Exception e)
            {
                return "error";
            }  
        }
    }
}
