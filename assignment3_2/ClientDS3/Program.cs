﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Net.Mail;

namespace ClientDS3
{
    class Program
    {
       static private string username;
       static private string password;
       static string address;

        /*
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello", durable: false, exclusive: false, autoDelete: false, arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);
                    writeToFile(message);
                   // sendMail(message);
                };
               
                channel.BasicConsume(queue: "hello", noAck: true, consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        } */

       static void Main(string[] args)
       {
           var factory = new ConnectionFactory() { HostName = "localhost" };
           using (var connection = factory.CreateConnection())
           using (var channel = connection.CreateModel())
           {
               channel.ExchangeDeclare(exchange: "logs", type: "fanout");

               var queueName = channel.QueueDeclare(readQueueName(),true,false,false,null);

               channel.QueueBind(queue: queueName, exchange: "logs", routingKey: "");

               Console.WriteLine(" [*] Waiting for logs.");

               var consumer = new EventingBasicConsumer(channel);
               consumer.Received += (model, ea) =>
               {
                   var body = ea.Body;
                   var message = Encoding.UTF8.GetString(body);
                   Console.WriteLine(" [x] {0}", message);
                   writeToFile(message);
               };
               channel.BasicConsume(queue: queueName, noAck: true, consumer: consumer);

               Console.WriteLine(" Press [enter] to exit.");
               Console.ReadLine();
           }
       }


        private static void writeToFile(string message)
        {
            using (System.IO.StreamWriter file =
               new System.IO.StreamWriter(@"C:\Users\Ionut\Desktop\DS3Data\DvdList.txt", true))
            {
                file.WriteLine(message);
            }
        }

        private static void readUserData()
        {
            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader(@"C:\Users\Ionut\Desktop\DS3Data\userData.txt");
            username = file.ReadLine();
            password = file.ReadLine();
            address = file.ReadLine();
            file.Close();

        }

        private static string readQueueName()
        {
            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader(@"file.txt");
            string queueName = "queue";
             queueName = file.ReadLine();
            file.Close();
            return queueName;
        }

        private static void sendMail(string message)
        {
            readUserData();
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(username, password);
            MailMessage mm = new MailMessage(address, address, "DvdList", message);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            client.Send(mm);       
        }

    }
}
