﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using Remote2;

namespace Assignment2Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private ServiceClass sc1;

        private void Form1_Load(object sender, EventArgs e)
        {
            sc1= (ServiceClass)Activator.GetObject(typeof(ServiceClass),
   "tcp://localhost:" + 9937 + "/ServiceClass1");
        }

        //tax
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int engineSize = int.Parse(textBox1.Text);
                int year = int.Parse(textBox2.Text);
                int price1 = int.Parse(textBox3.Text);
                double price = (double)price1;
                if (year < 2000 || price1 < 1 || engineSize < 1)
                {
                    textBox4.Clear();
                    textBox4.AppendText("input error");
                    return;
                }

                Car c1 = new Car(year, engineSize, price);
                double tax = sc1.tax(c1);
                textBox4.Clear();
                textBox4.AppendText("TAX = " + tax);
            }
            catch(FormatException er)
            {
                textBox4.Clear();
                textBox4.AppendText("format exception");
            }
           
        }

        //sellPrice
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int engineSize = int.Parse(textBox1.Text);
                int year = int.Parse(textBox2.Text);
                int price1 = int.Parse(textBox3.Text);
                double price = (double)price1;
                if (year < 2009 || price1 < 1 || engineSize < 1)
                {
                    textBox4.Clear();
                    textBox4.AppendText("input error");
                    return;
                }
                Car c1 = new Car(year, engineSize, price);
                double sellPrice = sc1.sellPrice(c1);
                textBox4.Clear();
                textBox4.AppendText("SellPrice = " + sellPrice);
            }
            catch(FormatException er)
            {
                textBox4.Clear();
                textBox4.AppendText("format exception");
            }
        }


    }
}
