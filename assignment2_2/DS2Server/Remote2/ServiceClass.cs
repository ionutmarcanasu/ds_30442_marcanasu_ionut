﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remote2
{

    public class ServiceClass : System.MarshalByRefObject
    {
        public ServiceClass()
        { }

        public double tax(Car car1)
        {
            double d1 = car1.getEngineSize() / 200;
            d1 = d1 * sum1(car1.getEngineSize());
            return d1;
        }

        private int sum1(int size)
        {
            if (size < 1600)
                return 8;
            if (size < 2000)
                return 16;
            if (size < 2600)
                return 72;
            if (size < 3000)
                return 144;

            return 290;
        }

        public double sellPrice(Car car1)
        {
            double d1 = car1.getPrice() / 7;
            int i2 = 2015 - car1.getYear();
            d1 = car1.getPrice() - d1 * i2;
            return d1;
        }

    }
}
