﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remote2
{
    [Serializable]
  public  class Car 
    {
        int year;
        int engineSize;
        double price;

       private Car()
       {
       }

        public Car(int year, int engineSize, double price)
        {
            this.year = year;
            this.engineSize = engineSize;
            this.price = price;

        }

        public int getYear()
        {
            return this.year;
        }

        public int getEngineSize()
        {
            return this.engineSize;
        }

        public double getPrice()
        {
            return this.price;
        }

        public void setYear(int year)
        {
            this.year = year;
        }

         public void setEngineSize(int size)
        {
            this.engineSize = size;
        }

         public void setPrice(double price)
        {
            this.price = price;
        }

    }
}
