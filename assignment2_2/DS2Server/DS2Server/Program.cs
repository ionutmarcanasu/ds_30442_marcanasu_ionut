﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
//using ROLibrary;
using Remote2;

namespace DS2Server
{
    class Program
    {
        static void Main(string[] args)
        {
            //The TcpServerChannel is one of the two types of channels
            //supported by .NET remoting. This will set up the port number
            //we want our object to respond to requests on, and the ChannelServices.
            //RegisterChannel will bind that port number to the TCP/IP stack
            //on the operating system.
            Console.WriteLine("Starting server...");
            Console.WriteLine("Opening channel ");

            BinaryServerFormatterSinkProvider provider = new
           BinaryServerFormatterSinkProvider();
            provider.TypeFilterLevel = TypeFilterLevel.Full;
            IDictionary props = new Hashtable();
            props["port"] = 9937;
            TcpChannel channel = new TcpChannel(props, null, provider);
            ChannelServices.RegisterChannel(channel);
            Console.WriteLine("Listening on channel " + channel);
            Console.WriteLine("Posting remote services");

       /*     RemotingConfiguration.RegisterWellKnownServiceType
             (typeof(Select1),
             "Select1", WellKnownObjectMode.Singleton);

            RemotingConfiguration.RegisterWellKnownServiceType
            (typeof(RemoteUser),
            "RemoteUser1", WellKnownObjectMode.Singleton);

            RemotingConfiguration.RegisterWellKnownServiceType
            (typeof(RemoteAdmin),
            "RemoteAdmin1", WellKnownObjectMode.Singleton);
        */
            RemotingConfiguration.RegisterWellKnownServiceType
          (typeof(ServiceClass),
          "ServiceClass1", WellKnownObjectMode.SingleCall);

          //  Select1 sel= new Select1();
        //    Console.WriteLine(sel.logIn("aa","a").ToString());

            Console.WriteLine("Server listening...");
            //do not close the console
            Console.ReadLine();

        }
    }
}
