/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servelets;

import controllers.FlightController;
import dataAccess.Cities;
import dataAccess.CitiesHelper;
import dataAccess.Flights;
import dataAccess.FlightsHelper;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static servelets.Servelet1.chelper;
import static servelets.Servelet1.fhelper;

/**
 *
 * @author Ionut
 */
public class FlightsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             
            
              String param1=request.getParameter("type");
        String forward1="deleteFlight";
         String forward2="Insert_Update";
        
         if(param1.equals(forward1))
         {
          processDeleteRequest(request,response);
         }
         else
          if(param1.equals(forward2))
         {
             fhelper=new FlightsHelper();
              chelper=new CitiesHelper();
            FlightController flightController=new FlightController();
          int checkCity1= flightController.insertFlight(request, fhelper,chelper);
            processRequest3(request,response,checkCity1);
         }
    }
    
    protected void processDeleteRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        FlightController flightController=new FlightController();
        int flightno=Integer.parseInt(request.getParameter("flightID"));
        fhelper=new FlightsHelper();
        flightController.deleteFlight(flightno,fhelper);
        processRequest3(request,response,1);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest2(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    // used for GET calls
     protected void processRequest2(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servelet1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println(" <br> <h1>There seems to be a login error... "  + "</h1> <br>");
            out.println(" <br>\n <a href=http://localhost:8080/DSWeb1/> <b>Back</b></a>\n" +"<br>");
            out.println("</body>");
            out.println("</html>");
            
        }
    }
     
     protected void processRequest3(HttpServletRequest request, HttpServletResponse response, int checkCity )
            throws ServletException, IOException {
        fhelper=new FlightsHelper();
         chelper=new CitiesHelper();
         List<Flights> flightsList=fhelper.getFlights();
          List<Cities> citiesList=chelper.getCities();
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
           out.println("<style> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%;} td, th { border: 1px solid #dddddd; text-align: left; padding: 8px;} tr:nth-child(even) { background-color: #dddddd;} </style>");
            out.println("<title>Servlet Servelet1</title>");            
            out.println("</head>");
           
            out.println("<body>");
            out.println("<table>");
            out.println(" <tr>\n" + "    <th>FlightNo</th>\n" + "    <th>PlaneType</th> \n" + "    <th>departureCity</th>\n" + " <th>departureDate</th> <th>arrivalCity</th> <th>arrivalDate</th>  </tr>");
            for(Flights fl : flightsList)
            {
            out.println("<tr>");
            out.println("<td>"+fl.getFlightNumber()+"</td>");
            out.println("<td>"+fl.getAirplanetype()+"</td>");
            out.println("<td>"+fl.getDeparturecity()+"</td>");
            out.println("<td>"+fl.getDepartureDate()+"</td>");
            out.println("<td>"+fl.getArrivalcity()+"</td>");
            out.println("<td>"+fl.getArrivalDate()+"</td>");
            out.println("/<tr>");
            }
              out.println("</table>");
               out.println("<br>");
               
               out.println("<br>");
               if(checkCity==0)
                     out.println("<p> Make sure the arrival and the departure cities exist! </p>");
                   
               out.println("<br>");
               
                //cities table
               out.println("<table>");
            out.println(" <tr>\n" + "    <th>City</th>\n" + "    <th>Latitude</th> \n" + "    <th>Longitude</th>\n" + " </tr>");
            for(Cities c1 : citiesList)
            {
            out.println("<tr>");
            out.println("<td>"+c1.getCityName()+"</td>");
            out.println("<td>"+c1.getLatitude()+"</td>");
            out.println("<td>"+c1.getLongitude()+"</td>");
            out.println("/<tr>");
            }
              out.println("</table>");
               out.println("<br>");
                out.println("<br>");
                
                {
                out.println("<form action=\"AdminServlet1\" method=\"post\">");
                out.println("<input type=\"submit\"  name=\"type\" value=\"CRUD_Flights\">");
                 out.println("<br>");
                  out.println("<br>");
                out.println("<form action=\"AdminServlet1\" method=\"post\">");
                out.println("<input type=\"submit\"  name=\"type\" value=\"CRUD_Cities\">");
                }
               
            out.println("</body>");
            out.println("</html>");
            
        }
    }
}
