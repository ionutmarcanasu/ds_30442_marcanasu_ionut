/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servelets;

import dataAccess.Cities;
import dataAccess.CitiesHelper;
import dataAccess.Flights;
import dataAccess.FlightsHelper;
import dataAccess.Users;
import dataAccess.UsersHelper;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ionut
 */
@WebServlet(name = "Servelet1", urlPatterns = {"/Servelet1"})
public class Servelet1 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
    /*
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servelet1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Servelet1 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
            
        }
    }
     */
    
    static UsersHelper helper;
    static FlightsHelper fhelper;
    static CitiesHelper chelper;

    //used for POST calls after login
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, int level)
            throws ServletException, IOException {
        fhelper=new FlightsHelper();
        chelper=new CitiesHelper();
         List<Flights> flightsList=fhelper.getFlights();
          List<Cities> citiesList=chelper.getCities();
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
           out.println("<style> table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%;} td, th { border: 1px solid #dddddd; text-align: left; padding: 8px;} tr:nth-child(even) { background-color: #dddddd;} </style>");
            out.println("<title>Servlet Servelet1</title>");            
            out.println("</head>");
           
            out.println("<body>");
            out.println("<table>");
            out.println(" <tr>\n" + "    <th>FlightNo</th>\n" + "    <th>PlaneType</th> \n" + "    <th>departureCity</th>\n" + " <th>departureDate</th> <th>arrivalCity</th> <th>arrivalDate</th>  </tr>");
            for(Flights fl : flightsList)
            {
            out.println("<tr>");
            out.println("<td>"+fl.getFlightNumber()+"</td>");
            out.println("<td>"+fl.getAirplanetype()+"</td>");
            out.println("<td>"+fl.getDeparturecity()+"</td>");
            out.println("<td>"+fl.getDepartureDate()+"</td>");
            out.println("<td>"+fl.getArrivalcity()+"</td>");
            out.println("<td>"+fl.getArrivalDate()+"</td>");
            out.println("/<tr>");
            }
              out.println("</table>");
               out.println("<br>");
           
               //cities table and admin forms
                out.println("<br>");
                if(level==2)
                {
                      out.println("<table>");
            out.println(" <tr>\n" + "    <th>City</th>\n" + "    <th>Latitude</th> \n" + "    <th>Longitude</th>\n" + " </tr>");
            for(Cities c1 : citiesList)
            {
            out.println("<tr>");
            out.println("<td>"+c1.getCityName()+"</td>");
            out.println("<td>"+c1.getLatitude()+"</td>");
            out.println("<td>"+c1.getLongitude()+"</td>");
            out.println("/<tr>");
            }
              out.println("</table>");
               out.println("<br>");
                    
                    
                out.println("<form action=\"AdminServlet1\" method=\"post\">");
                out.println("<input type=\"submit\"  name=\"type\" value=\"CRUD_Flights\">");
                 out.println("<br>");
                  out.println("<br>");
                out.println("<form action=\"AdminServlet1\" method=\"post\">");
                out.println("<input type=\"submit\"  name=\"type\" value=\"CRUD_Cities\">");
                }
                //end if
                if(level==1)
                {
                 out.println("<br>");
                  out.println("<br>");
                out.println("<form action=\"LocalTimeServlet\" method=\"post\">");
                 out.println("<p>Local Time</p>");
                  out.println("<br>");
               out.println("<input type=\"text\" name=\"location\">");
                out.println("<input type=\"submit\"  name=\"type\" value=\"Local_Time\">");
                 out.println("<br>");
                }
                
            out.println("</body>");
            out.println("</html>");
            
        }
    }

     //used for POST calls
     protected void loginRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         helper = new UsersHelper();
       String username=request.getParameter("username");
       String password=request.getParameter("password");
        List<Users> usersList=helper.getUsers();
       int found=0;
        for(Users us : usersList)
        {
         if((us.getUsername().equals(username))&&(us.getPassword().equals(password)))
          {  found=1;
              if((us.getLevel()==1)||(us.getLevel()==2))
                processRequest(request,response,us.getLevel());
            break;  
          }
        }
        if(found==0)
            processRequest2(request,response);
     }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       processRequest2(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        loginRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
   
    
        // used for GET calls
     protected void processRequest2(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servelet1</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println(" <br> <h1>There seems to be a login error... "  + "</h1> <br>");
            out.println(" <br>\n <a href=http://localhost:8080/DSWeb1/> <b>Back</b></a>\n" +"<br>");
            out.println("</body>");
            out.println("</html>");
            
        }
    }
}
