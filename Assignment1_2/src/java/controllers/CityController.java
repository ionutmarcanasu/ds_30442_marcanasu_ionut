/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import dataAccess.Cities;
import dataAccess.CitiesHelper;
import dataAccess.FlightsHelper;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ionut
 */
public class CityController {
    
    public CityController()
    {}

     public void deleteCity(String id,CitiesHelper chelper,FlightsHelper fhelper)
    {
     List<Cities> citiesList = null;
     citiesList=chelper.getCities();
     for(Cities c1 : citiesList)
     {
      if(c1.getCityName().equals(id))
      {
       fhelper.clearFlights(c1.getCityName());
       chelper.deleteCity(c1);
       break;
      }
     }
    }
    
      public void insertCity(HttpServletRequest request,CitiesHelper chelper) 
      {
        Cities c1=null;
        String name=request.getParameter("cityID");
        String latitude=request.getParameter("latitude");
        String longitude=request.getParameter("longitude");
        c1=new Cities(name,latitude,longitude);
        chelper.insertCity(c1);
      }
      
}
