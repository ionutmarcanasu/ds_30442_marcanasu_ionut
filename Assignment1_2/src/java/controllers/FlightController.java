/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import dataAccess.CitiesHelper;
import dataAccess.Flights;
import dataAccess.FlightsHelper;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ionut
 */
public class FlightController {
   // static FlightsHelper fhelper;
     
    public FlightController(){
     
            }
    
    public void deleteFlight(int id,FlightsHelper fhelper)
    {
       // fhelper=new FlightsHelper();
     List<Flights> flightsList = null;
     flightsList=fhelper.getFlights();
     for(Flights f1 : flightsList)
     {
      if(f1.getFlightNumber()==id)
      {
       fhelper.deleteFlight(f1);
       break;
      }
     }
    }
    
    public int insertFlight(HttpServletRequest request,FlightsHelper fhelper, CitiesHelper chelper) 
    {
        Flights f1=null;
     int flightNumber=Integer.parseInt(request.getParameter("flightID"));
      String airplanetype=request.getParameter("airType");
      String departurecity=request.getParameter("departureCity");
      String arrivalcity=request.getParameter("arrivalCity");
      DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm", Locale.ENGLISH);
        try {
            // Date date = format.parse(string);
            Date departureDate=format.parse(request.getParameter("departureDate"));
             Date arrivalDate=format.parse(request.getParameter("arrivalDate"));
              f1=new Flights(flightNumber,airplanetype,departurecity,departureDate,arrivalcity,arrivalDate);
              int checkCity=chelper.checkCities(departurecity,arrivalcity);
              if(checkCity==0)
                  return 0;
              fhelper.insertFlight(f1);
        } catch (ParseException ex) {
            Logger.getLogger(FlightController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 1;
      
    }
    
     
}