/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import dataAccess.Cities;
import dataAccess.CitiesHelper;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Ionut
 */
public class LocalTimeController {
   
    
    private String getTime(String latitude, String longitude) throws MalformedURLException, IOException, ParserConfigurationException, SAXException
     {
         String time="aa";
         
        
            URL url = new URL("http://new.earthtools.org/timezone/"+latitude+"/"+longitude);
           URLConnection conn = null;
             
                 conn = url.openConnection();
             
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;
            
                 builder = factory.newDocumentBuilder();
            
            Document doc = null;
             
                 doc = (Document) builder.parse(conn.getInputStream());
             
         //  NodeList nList= doc.getElementsByTagName("offset");
         //  Node nNode = nList.item(0);
           time=doc.getElementsByTagName("localtime").item(0).getChildNodes().item(0).getNodeValue();
     return time;
     }
    
    public String getLocalTime(String cityName,CitiesHelper chelper)
    {
      String latitude=null;
      String longitude=null; 
      String time = "aa";
     List<Cities> citiesList = null;
     citiesList=chelper.getCities();
     for(Cities c1 : citiesList)
     {
      if(c1.getCityName().equals(cityName))
      {
       latitude=c1.getLatitude();
       longitude=c1.getLongitude();
       break;
      }
     }
        try {
            time=getTime(latitude,longitude);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(LocalTimeController.class.getName()).log(Level.SEVERE, null, ex);
        }
     return time;
    }
}
