/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 *
 * @author Ionut
 */
public class UsersHelper {
      Session session = null;

    public UsersHelper() {
           this.session = HibernateUtil.getSessionFactory().getCurrentSession();
          //  session = HibernateUtil.getSessionFactory().openSession();
    }
    
     public List getUsers()
    {
     
     List<Users> usersList = null;
    try {
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Users as users ");
        usersList = (List<Users>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return usersList;
    }
     
    public Users getUserByName(String nume)
    {
        Users us=null;
      try {
      //us=  (Users) session.get(us.getClass(), nume);
          us=(Users) session.get(us.getClass(), nume);
    } catch (Exception e) {
        e.printStackTrace();
    }
      return us;
    }  
}
