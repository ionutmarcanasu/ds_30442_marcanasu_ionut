/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Ionut
 */
public class CitiesHelper {
     Session session = null;
     
     public CitiesHelper() {
           this.session = HibernateUtil.getSessionFactory().getCurrentSession();
          //  session = HibernateUtil.getSessionFactory().openSession();
    }
     
       public List getCities()
    {
     
     List<Cities> citiesList = null;
    try {
         this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Cities as cities ");
        citiesList = (List<Cities>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return citiesList;
    }
       
       
       public void deleteCity(Cities fl)
       {
        try {
            this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.delete(fl);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
       
       public void insertCity(Cities fl)
       {
         try {
            this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(fl);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
       
       public int checkCities(String city1, String city2)
       {
         if(checkCity(city1)&&checkCity(city2))
        return 1;
         else
             return 0;
       }
       
       public boolean checkCity(String cityName)
       {
       List<Cities> citiesList=getCities();
         for(Cities c1 : citiesList)
         {
          if(c1.getCityName().equals(cityName))
              return true;
         }
       
       return false;
       }
               
       
}
