/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dataAccess;

/**
 *
 * @author Ionut
 */

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class FlightsHelper {
     Session session = null;
     
     public FlightsHelper() {
           this.session = HibernateUtil.getSessionFactory().getCurrentSession();
          //  session = HibernateUtil.getSessionFactory().openSession();
    }
     
       public List getFlights()
    {
     
     List<Flights> flightsList = null;
    try {
         this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Flights as flights ");
        flightsList = (List<Flights>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return flightsList;
    }
       
       
       public void deleteFlight(Flights fl)
       {
        try {
            this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.delete(fl);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
       
       public void insertFlight(Flights fl)
       {
         try {
            this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction  tx = session.beginTransaction();
            session.saveOrUpdate(fl);
            tx.commit();
          } catch (HibernateException e) {
        e.printStackTrace();
        }
       }
       
        public List getCities()
    {
     
     List<Cities> citiesList = null;
    try {
         this.session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        Query q = session.createQuery ("from Cities as cities ");
        citiesList = (List<Cities>) q.list();
      tx.commit();
    } catch (HibernateException e) {
        e.printStackTrace();
    }
    return citiesList;
    }
       
      public void clearFlights(String cityName)
      {
        List<Flights> fList=getFlights();
        for(Flights fl : fList)
        {
         if(fl.getArrivalcity().equalsIgnoreCase(cityName)||fl.getDeparturecity().equalsIgnoreCase(cityName))
         {
          deleteFlight(fl);
         }
        }
      }
}
