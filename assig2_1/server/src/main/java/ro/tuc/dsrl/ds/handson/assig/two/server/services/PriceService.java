/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellPrice;

/**
 *
 * @author Ionut
 */
public class PriceService implements ISellPrice{

    @Override
    public double computePrice(Car c) {
        if (c.getPurchasePrice()<= 0) {
			throw new IllegalArgumentException("Car price must be positive.");
		}
        double ps=0;
        double pc=c.getPurchasePrice();
        int p1=(int) (pc/7);
        double p2=2015-c.getYear();
        double p3=p1*p2;
        ps=pc-p3;
        return ps;
    }
    
    
}
